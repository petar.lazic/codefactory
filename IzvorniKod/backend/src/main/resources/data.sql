-- klijenti

insert into korisnik(id,ime,prezime,kontakt_broj,e_posta,korisnicko_ime,lozinka,broj_upita,iban, vrsta_korisnika)
values(10001,'Marko', 'Markic', '09244020', 'marmar@gmail.com', 'marmar', '$2a$10$y233SAWV2X2FcDewqzDc/uIFtwRZdjDzJXBPaG3Zr.CqR3VLpEQ4u', 0, 'HR9230449494030339303', 'klijent');

insert into korisnik(id,ime,prezime,kontakt_broj,e_posta,korisnicko_ime,lozinka,broj_upita,iban, vrsta_korisnika)
values(10002,'Ivan', 'Ivanić', '09202020', 'iviv@gmail.com', 'iviv', '$2a$10$y233SAWV2X2FcDewqzDc/uIFtwRZdjDzJXBPaG3Zr.CqR3VLpEQ4u', 0, 'HR9230449494030339303', 'klijent');

insert into korisnik(id,ime,prezime,kontakt_broj,e_posta,korisnicko_ime,lozinka,broj_upita,iban, vrsta_korisnika)
values(10003,'Josip', 'Josipović', '092300390', 'josjos@gmail.com', 'josjos', '$2a$10$y233SAWV2X2FcDewqzDc/uIFtwRZdjDzJXBPaG3Zr.CqR3VLpEQ4u', 0, 'HR9230449494030339303', 'klijent');

insert into korisnik(id,ime,prezime,kontakt_broj,e_posta,korisnicko_ime,lozinka,broj_upita,iban, vrsta_korisnika)
values(10004,'Petar', 'Petrić', '093880309', 'petpet@gmail.com', 'petpet', '$2a$10$y233SAWV2X2FcDewqzDc/uIFtwRZdjDzJXBPaG3Zr.CqR3VLpEQ4u', 0, 'HR9230449494030339303', 'klijent'); --pass: klijentpass

-- djelatnici

insert into korisnik(id,ime,prezime,kontakt_broj,e_posta,korisnicko_ime,lozinka,administrator, vrsta_korisnika) -- administrator
values(10005,'Ante', 'Antić', '0989939903', 'antant@gmail.com', 'antant', '$2a$10$arIGY0z1dO0RmaqjsU/1JeD9F6UT/fUgQRnP9n36zHV7ceqjeR.le', true , 'djelatnik'); --pass: admini

insert into korisnik(id,ime,prezime,kontakt_broj,e_posta,korisnicko_ime,lozinka,administrator, vrsta_korisnika)
values(10006,'Filip', 'Filipović', '093003977', 'filfil@gmail.com', 'filfil', '$2a$10$arIGY0z1dO0RmaqjsU/1JeD9F6UT/fUgQRnP9n36zHV7ceqjeR.le', false , 'djelatnik');

-- dugovi

--INSERT INTO DUG(id, datum_zaduzenja, iznos, rok_otplate) VALUES (1001, CURRENT_TIMESTAMP, 20000, CURRENT_TIMESTAMP + 90);
--INSERT INTO DUG(id, datum_zaduzenja, iznos, rok_otplate) VALUES (1002, CURRENT_TIMESTAMP, 20, CURRENT_TIMESTAMP + 150);

-- predmeti

--INSERT INTO PREDMET(id, cijena, datum_uvrstenja, ime_predmeta, posebna_ponuda, u_ponudi, dug_id, klijent_id)
--VALUES(1001, 20, CURRENT_TIMESTAMP, 'olovka', TRUE, TRUE, null, 10001)

--INSERT INTO PREDMET(id, cijena, datum_uvrstenja, ime_predmeta, posebna_ponuda, u_ponudi, dug_id, klijent_id)
--VALUES(1002, 1000, CURRENT_TIMESTAMP, 'pernica', TRUE, TRUE, null, 10002)

