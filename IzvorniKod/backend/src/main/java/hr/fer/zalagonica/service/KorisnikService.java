package hr.fer.zalagonica.service;


import hr.fer.zalagonica.domain.Korisnik;
import java.util.Optional;

public interface KorisnikService {
    Optional<Korisnik> findByUsername(String username);
}
