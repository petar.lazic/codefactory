package hr.fer.zalagonica.rest;


import hr.fer.zalagonica.domain.Klijent;
import hr.fer.zalagonica.domain.Upit;
import hr.fer.zalagonica.service.UpitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/upiti")
@CrossOrigin(origins="http://localhost:3000")
public class UpitController {

    @Autowired
    private UpitService upitService;

    @GetMapping("")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public List<Upit> dohvatiUpite() {
        return upitService.dohvatiUpite();
    }

    @GetMapping("jedan/{id}")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public Optional<Upit> dohvatiJedanUpit(@PathVariable Long id) {
        return upitService.findById(id);
    }

    @PostMapping("posaljiupit")
    public Upit kreirajUpit(@RequestBody Upit upit){
        return upitService.kreirajUpit(upit);
    }

    @PostMapping("izbrisi")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public String izbrisiUpit(@RequestBody Long id){
        return upitService.izbrisiUpit(id);
    }

    @PostMapping("azuriraj")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public Upit azurirajUpit(@RequestBody Upit upit){
        return upitService.azurirajUpit(upit);
    }

    @PostMapping("odgovori")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public void odgovoriNaUpit(@RequestParam Long id, @RequestParam String tekstMaila){
        upitService.odgovoriNaUpit(id,tekstMaila);
    }


}
