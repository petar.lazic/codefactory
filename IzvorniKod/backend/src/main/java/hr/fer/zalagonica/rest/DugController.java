package hr.fer.zalagonica.rest;


import hr.fer.zalagonica.domain.Dug;
import hr.fer.zalagonica.service.DugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/dugovi")
@CrossOrigin(origins="http://localhost:3000")
public class DugController {

    @Autowired
    private DugService dugService;

    @GetMapping("")
    @Secured("ROLE_ADMIN")
    public List<Dug> dohvatiDugove() {
        return dugService.dohvatiDugove();
    }

    @PostMapping("registriraj")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public Dug unesiDug(@RequestBody Dug dug, @AuthenticationPrincipal User u) {
        return dugService.kreirajDug(dug);
    }


    @PostMapping("izbrisi")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public String izbrisiDug(@RequestBody Long id) {
        return dugService.izbrisiDug(id);
    }

    @PostMapping("azuriraj")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public Dug azurirajDug(@RequestBody Dug dug) {
        return dugService.azurirajDug(dug);
    }

    @GetMapping("/dug/{id}")
    public Dug dohvatiPoPredmetu(@PathVariable Long id) {
        return dugService.dohvatiDug(id);
    }

}
