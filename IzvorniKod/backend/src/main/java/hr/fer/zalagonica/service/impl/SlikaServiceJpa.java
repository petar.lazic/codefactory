package hr.fer.zalagonica.service.impl;


import hr.fer.zalagonica.dao.SlikaRepository;
import hr.fer.zalagonica.domain.Dug;
import hr.fer.zalagonica.domain.Slika;
import hr.fer.zalagonica.service.RequestDeniedException;
import hr.fer.zalagonica.service.SlikaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
public class SlikaServiceJpa implements SlikaService {

    @Autowired
    private SlikaRepository slikaRepo;

    @Override
    public List<Slika> dohvatiSlike() {
        return slikaRepo.findAll();
    }

    @Override
    public Optional<Slika> findById(Long id) {
        Assert.notNull(id, "ID must be given");
        return slikaRepo.findById(id);
    }

    @Override
    public Slika kreirajSliku(Slika slika) {
        Assert.notNull(slika, "Pic object must be given");
        Assert.isNull(slika.getId(), "Pic ID must be null, not " + slika.getId());

        String put=slika.getPut();
        return slikaRepo.save(slika);
    }

    @Override
    public String izbrisiSliku(Long id) {
        Slika slika = slikaRepo.findById(id).orElseThrow(() -> new RequestDeniedException("No pic with id " + id));
        slikaRepo.deleteById(slika.getId());
        return "Pic '" + id + "' has been succesfully removed from database.";
    }

    @Override
    public Slika azurirajSliku(Slika slika) {
        Assert.notNull(slika, "Pic object must be given");

        Long id = slika.getId();
        Slika novaSlika = slikaRepo.findById(id).orElseThrow(()->new RequestDeniedException(("No pic with id " + id)));

        novaSlika.setPut(slika.getPut());
        return slika;
    }
}
