package hr.fer.zalagonica.service.impl;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import hr.fer.zalagonica.dao.TransakcijaRepository;
import hr.fer.zalagonica.domain.Klijent;
import hr.fer.zalagonica.domain.Transakcija;
import hr.fer.zalagonica.service.RequestDeniedException;
import hr.fer.zalagonica.service.TransakcijaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class TransakcijaServiceJpa implements TransakcijaService {


    @Autowired
    private TransakcijaRepository transakcijaRepo;

    @Override
    public List<Transakcija> dohvatiTransakcije() {
        return transakcijaRepo.findAll();
    }

    @Override
    @Transactional
    public Transakcija kreirajTransakciju(Transakcija transakcija) {
        Assert.notNull(transakcija, "Transaction object must be given");
        Assert.isNull(transakcija.getId(), "Transaction ID must be null, not " + transakcija.getId());

        Assert.notNull(transakcija.getPredmet(), "Item must be given");
        Assert.notNull(transakcija.getVrstaTransakcije(), "Transaction type must be given");
        if(transakcija.getDatum()==null)
            transakcija.setDatum(LocalDateTime.now());

        return transakcijaRepo.save(transakcija);
    }

    @Override
    public String izbrisiTransakciju(Long id) {
        Transakcija transakcija = transakcijaRepo.findById(id).orElseThrow(() -> new RequestDeniedException("No transaction with id " + id));
        transakcijaRepo.deleteById(transakcija.getId());
        return "Transaction '" + transakcija.getId() + "' has been succesfully removed from database.";
    }

    @Override
    public Transakcija azurirajTransakciju(Transakcija transakcija) {
        Assert.notNull(transakcija, "Transaction object must  be given");
        Assert.notNull(transakcija.getId(),"Transaction ID can't be null");
        Assert.notNull(transakcija.getIznos(),"Transaction value must not be null");

        Long id = transakcija.getId();
        Transakcija novaTransakcija = transakcijaRepo.findById(id).orElseThrow(() -> new RequestDeniedException(("No transaction with id " + id)));

        novaTransakcija.setPredmet(transakcija.getPredmet());
        novaTransakcija.setDatum(transakcija.getDatum());
        novaTransakcija.setIznos(transakcija.getIznos());
        novaTransakcija.setKlijent(transakcija.getKlijent());
        novaTransakcija.setDjelatnik(transakcija.getDjelatnik());
        novaTransakcija.setVrstaTransakcije(transakcija.getVrstaTransakcije());

        return transakcijaRepo.save(novaTransakcija);
    }

    public Optional<List<Transakcija>> nadjiKlijentoveTransakcije(Klijent klijent){
        Assert.notNull(klijent, "Client object must not be null");
        Assert.notNull(klijent.getId(), "Client ID must not be null");

        return transakcijaRepo.findByKlijent(klijent);
    }
}
