package hr.fer.zalagonica.dao;

import hr.fer.zalagonica.domain.Djelatnik;
import hr.fer.zalagonica.domain.Klijent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DjelatnikRepository extends JpaRepository<Djelatnik, Long> {
    List<Djelatnik> findAll();
    Optional<Djelatnik> findById(Long id);
    Optional<Djelatnik> findByKorisnickoIme(String ime);
    void deleteById(Long id);
}
