package hr.fer.zalagonica.service;

import hr.fer.zalagonica.domain.Klijent;
import hr.fer.zalagonica.domain.Predmet;
import org.springframework.stereotype.Service;

import java.util.List;

public interface PredmetService {
    List<Predmet> dohvatiPredmete();
    Predmet kreirajPredmet(Predmet predmet);
    String izbrisiPredmet(Long id);
    Predmet azurirajPredmet(Predmet predmet);
    List<Predmet> dohvatiPoKljucnimRijecima(String trazeneRijeci);
    List<Predmet> dohvatiPredmeteUPonudi();
    List<Predmet> dohvatiPredmeteUPosebnojPonudi();
    void staviUPonudu(Predmet predmet);
    void staviUPosebnuPonudu(Predmet predmet);
    Predmet dohvatiPredmet(Long id, String username, String role);
	List<Predmet> dohvatiPredmeteKlijenta(Klijent klijent);
}
