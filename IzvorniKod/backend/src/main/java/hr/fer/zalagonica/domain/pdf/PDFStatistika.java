package hr.fer.zalagonica.domain.pdf;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import hr.fer.zalagonica.dao.TransakcijaRepository;
import hr.fer.zalagonica.domain.Djelatnik;
import hr.fer.zalagonica.domain.Klijent;
import hr.fer.zalagonica.domain.Transakcija;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;
import java.util.List;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

public class PDFStatistika {

    private List<Transakcija> transakcije;

    public ResponseEntity<byte[]> generirajPDF(List<Transakcija> trans, int month, int year) {
        try {
            //this.transakcije = transakcijaRepo.findByDatum_MonthValueAndDatum_Year(month, year);
            this.transakcije = trans;

            Document document = new Document(PageSize.A4.rotate());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
            SimpleDateFormat footerformat = new SimpleDateFormat("dd.MM.yyyy. HH:mm:ss");

            String statistikaFile = System.getProperty("user.home") + "/Documents/Statistika_zalagaonica_" + simpleDateFormat.format(new Date()) + ".pdf";

            PdfWriter pdfWriter = PdfWriter.getInstance(document,
                    new FileOutputStream(statistikaFile));
            TableFooter event = new TableFooter();
            pdfWriter.setPageEvent(event);
            event.setFooter("ZALAGAONICA " + footerformat.format(new Date()));

            document.open();

            BaseFont bf1 = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.EMBEDDED);

            Font fontNaslova = new Font(bf1, 38);
            Paragraph naslov = new Paragraph("ZALAGAONICA", fontNaslova);
            naslov.setLeading(45);
            naslov.setAlignment(Element.ALIGN_LEFT);
            document.add(naslov);

            Paragraph ugovor = new Paragraph("STATISTIKA ZA " + month + "/" + year, new Font(bf1, 16));
            ugovor.setLeading(100);
            ugovor.setAlignment(Element.ALIGN_CENTER);
            document.add(ugovor);

            StringBuilder bldr = new StringBuilder();
            bldr.append("Broj transakcija: " + transakcije.size());
            bldr.append(", Ukupni iznos: " + zbroji());
            Paragraph data = new Paragraph(bldr.toString(), new Font(bf1, 14));
            document.add(data);

            PdfPTable table = kreirajTablicu(new Font(bf1, 14));
            document.add(table);

            document.close();

            Path pathname = Paths.get(statistikaFile);
            byte[] bytes = Files.readAllBytes(pathname);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.parseMediaType("application/pdf"));
            headers.add("content-disposition", "inline; filename=" + statistikaFile);

            headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
            ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(bytes, headers, HttpStatus.OK);
            return response;

        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    class TableFooter extends PdfPageEventHelper {
        /**
         * The header text.
         */
        String footer;
        /**
         * The template with the total number of pages.
         */
        PdfTemplate total;

        /**
         * Allows us to change the content of the footer.
         *
         * @param footer The new header String
         */
        public void setFooter(String footer) {
            this.footer = footer;
        }

        /**
         * Creates the PdfTemplate that will hold the total number of pages.
         *
         * @see com.itextpdf.text.pdf.PdfPageEventHelper#onOpenDocument(
         *com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
         */
        public void onOpenDocument(PdfWriter writer, Document document) {
            total = writer.getDirectContent().createTemplate(30, 16);
        }

        /**
         * Adds a header to every page
         *
         * @see com.itextpdf.text.pdf.PdfPageEventHelper#onEndPage(
         *com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
         */
        public void onEndPage(PdfWriter writer, Document document) {
            PdfPTable table = new PdfPTable(3);
            try {
                table.setWidths(new int[]{24, 24, 2});
                table.setTotalWidth(document.right(document.rightMargin()) - document.left(document.leftMargin()));
                table.setLockedWidth(true);
                table.getDefaultCell().setFixedHeight(20);
                table.getDefaultCell().setBorder(Rectangle.BOTTOM);
                table.addCell(footer);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(String.format("%d /", writer.getPageNumber()));
                PdfPCell cell = new PdfPCell(Image.getInstance(total));
                cell.setBorder(Rectangle.BOTTOM);
                table.addCell(cell);
                table.writeSelectedRows(0, -1, document.left(document.leftMargin()), table.getTotalHeight() + 15, writer.getDirectContent());
            } catch (DocumentException de) {
                throw new ExceptionConverter(de);
            }
        }

        /**
         * Fills out the total number of pages before the document is closed.
         *
         * @see com.itextpdf.text.pdf.PdfPageEventHelper#onCloseDocument(
         *com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
         */
        public void onCloseDocument(PdfWriter writer, Document document) {
            ColumnText.showTextAligned(total, Element.ALIGN_LEFT,
                    new Phrase(String.valueOf(writer.getPageNumber())),
                    2, 2, 0);
        }
    }

    public PdfPTable kreirajTablicu(Font f) throws DocumentException {
        PdfPTable table = new PdfPTable(6);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        table.setWidthPercentage(100);
        table.setSpacingBefore(30);
        table.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);


        table.addCell(new PdfPCell(new Phrase("PREDMET", f)));
        table.addCell(new PdfPCell(new Phrase("KLIJENT", f)));
        table.addCell(new PdfPCell(new Phrase("DJELATNIK", f)));
        table.addCell(new PdfPCell(new Phrase("VRSTA", f)));
        table.addCell(new PdfPCell(new Phrase("DATUM", f)));
        table.addCell(new PdfPCell(new Phrase("IZNOS", f)));

        for (Transakcija transakcija : transakcije) {
            table.addCell(new PdfPCell(new Phrase(transakcija.getPredmet().getImePredmeta(), f)));
            Klijent klijent = transakcija.getKlijent();
            Djelatnik djelatnik = transakcija.getDjelatnik();
            table.addCell(new PdfPCell(new Phrase(klijent.getIme() + " " + klijent.getPrezime(), f)));
            table.addCell(new PdfPCell(new Phrase(djelatnik.getIme() + " " + djelatnik.getPrezime(), f)));
            table.addCell(new PdfPCell(new Phrase(transakcija.getVrstaTransakcije(), f)));
            table.addCell(new PdfPCell(new Phrase(formatter.format(transakcija.getDatum()), f)));
            table.addCell(new PdfPCell(new Phrase(transakcija.getIznos().toString(), f)));

        }


        return table;
    }

    public double zbroji() {
        double suma = 0.0;
        for (Transakcija tr : this.transakcije) {
            suma += tr.getIznos();
        }
        return suma;
    }
}
