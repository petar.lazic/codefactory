package hr.fer.zalagonica.service;

import hr.fer.zalagonica.domain.Klijent;
import hr.fer.zalagonica.domain.Transakcija;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

public interface TransakcijaService {
    List<Transakcija> dohvatiTransakcije();
    Transakcija kreirajTransakciju(Transakcija transakcija);
    String izbrisiTransakciju(Long id);
    public Transakcija azurirajTransakciju(Transakcija transakcija);
    Optional<List<Transakcija>> nadjiKlijentoveTransakcije(Klijent klijent);
}
