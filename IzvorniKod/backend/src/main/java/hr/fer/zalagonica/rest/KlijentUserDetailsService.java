package hr.fer.zalagonica.rest;

import hr.fer.zalagonica.dao.KorisnikRepository;
import hr.fer.zalagonica.domain.Djelatnik;
import hr.fer.zalagonica.domain.Klijent;
import hr.fer.zalagonica.domain.Korisnik;
import hr.fer.zalagonica.service.KlijentService;
import hr.fer.zalagonica.service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthoritiesContainer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.security.core.authority.AuthorityUtils.commaSeparatedStringToAuthorityList;

@Service
public class KlijentUserDetailsService implements UserDetailsService
{

    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Korisnik korisnik = korisnikService.findByUsername(username).orElseThrow(()->new UsernameNotFoundException("No user " + username + "."));

        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialNotExpired = true;
        boolean accountNonLocked = true;

        return new User(korisnik.getKorisnickoIme(), korisnik.getLozinka(), enabled, accountNonExpired, credentialNotExpired, accountNonLocked, authorities(username));
    }

    private List<GrantedAuthority> authorities(String username) {
        Korisnik korisnik = korisnikService.findByUsername(username).orElseThrow(()->new UsernameNotFoundException("No user " + username + "."));

       if(korisnik instanceof Djelatnik) {
           Djelatnik djelatnik = (Djelatnik) korisnik;
           if(djelatnik.isAdministrator()) return commaSeparatedStringToAuthorityList("ROLE_ADMIN");
           else return commaSeparatedStringToAuthorityList("ROLE_DJELATNIK");
       } else {
           return commaSeparatedStringToAuthorityList("ROLE_KLIJENT");
       }
    }
}
