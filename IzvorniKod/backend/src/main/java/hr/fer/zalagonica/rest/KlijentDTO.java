package hr.fer.zalagonica.rest;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class KlijentDTO {

    @NotNull
    @NotEmpty
    @Size(min = 3, max = 45)
    private String ime;

    @NotNull
    @NotEmpty
    @Size(min = 3, max = 45)
    private String prezime;

    @Size(min = 3, max = 45)
    private String kontaktBroj;

    @Size(min = 3, max = 45)
    @NotEmpty
    private String ePosta;

    @Size(min = 3, max = 45)
    @NotEmpty
    private String korisnickoIme;

    @NotNull
    @NotEmpty
    @Size(min = 6)
    private String lozinka;

    @NotNull
    @NotEmpty
    @Size(min = 6)
    private String matchingLozinka;

    @Size(min=21, max=21)
    private String iban;

    public String getPrezime() {
        return prezime;
    }

    public String getIme() {
        return ime;
    }

    public String getKontaktBroj() {
        return kontaktBroj;
    }

    public String getePosta() {
        return ePosta;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setePosta(String ePosta) {
        this.ePosta = ePosta;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public void setKontaktBroj(String kontaktBroj) {
        this.kontaktBroj = kontaktBroj;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public void setMatchingLozinka(String matchingLozinka) {
        this.matchingLozinka = matchingLozinka;
    }

    public String getMatchingLozinka() {
        return matchingLozinka;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

}
