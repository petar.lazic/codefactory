package hr.fer.zalagonica.dao;

import hr.fer.zalagonica.domain.Korisnik;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface KorisnikRepository extends JpaRepository<Korisnik, Long> {
    Optional<Korisnik> findById(Long id);
    int countByKorisnickoIme(String korisnickoIme);
    int countByEPosta(String ePosta);
    Optional<Korisnik> findByKorisnickoIme(String username);
}
