package hr.fer.zalagonica.dao;

import hr.fer.zalagonica.domain.Klijent;
import hr.fer.zalagonica.domain.Slika;
import hr.fer.zalagonica.domain.Transakcija;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface TransakcijaRepository extends JpaRepository<Transakcija, Long> {
    List<Transakcija> findAll();
    Optional<Transakcija> findById(Long id);
    Optional<Transakcija> findByVrstaTransakcije(String vrstaTransakcije);
    Optional<Transakcija> findByDatum(LocalDateTime datum);
    Optional<List<Transakcija>> findByKlijent(Klijent klijent);
    void deleteById(Long id);
    @Query("select e from Transakcija e where month(e.datum) = ?1 and year(e.datum) = ?2")
    List<Transakcija> getByYearAndMonth(int m, int y);
}
