package hr.fer.zalagonica.service;

import hr.fer.zalagonica.domain.Klijent;
import hr.fer.zalagonica.domain.Upit;

import java.util.List;
import java.util.Optional;

public interface UpitService {

    List<Upit> dohvatiUpite();
    Optional<Upit> findById(Long id);
    Upit kreirajUpit(Upit upit);
    String izbrisiUpit(Long id);
    void odgovoriNaUpit(Long id, String tekstMaila);
    Upit azurirajUpit(Upit upit);

}
