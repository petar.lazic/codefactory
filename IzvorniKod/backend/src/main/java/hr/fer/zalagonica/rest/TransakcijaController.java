package hr.fer.zalagonica.rest;


import hr.fer.zalagonica.domain.Klijent;
import hr.fer.zalagonica.domain.Predmet;
import hr.fer.zalagonica.domain.Transakcija;
import hr.fer.zalagonica.service.TransakcijaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/transakcije")
@CrossOrigin(origins="http://localhost:3000")
public class TransakcijaController {

    @Autowired
    private TransakcijaService transakcijaService;

    @GetMapping("")
    @Secured("ROLE_ADMIN")
    public List<Transakcija> dohvatiTransakcije() {
        return transakcijaService.dohvatiTransakcije();
    }

    @PostMapping("kreiraj")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public Transakcija kreirajTransakciju(@RequestBody Transakcija transakcija) {
        return transakcijaService.kreirajTransakciju(transakcija);
    }

    @PostMapping("izbrisi")
    @Secured("ROLE_ADMIN")
    public String izbrisiTransakciju(@RequestBody long id) {
        return transakcijaService.izbrisiTransakciju(id);
    }

    @PostMapping("azuriraj")
    @Secured("ROLE_ADMIN")
    public Transakcija azurirajTransakciju(@RequestBody Transakcija transakcija) {
        return transakcijaService.azurirajTransakciju(transakcija);
    }

    @PostMapping("dohvatiklijentove")
    @Secured({"ROLE_KLIJENT", "ROLE_DJELATNIK", "ROLE_ADMIN"})
    public Optional<List<Transakcija>> dohvatiKlijentoveTransakcije(@RequestBody Klijent klijent){
        return transakcijaService.nadjiKlijentoveTransakcije(klijent);
    }

}
