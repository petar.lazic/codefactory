package hr.fer.zalagonica.dao;

import hr.fer.zalagonica.domain.Klijent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface KlijentRepository extends JpaRepository<Klijent, Long> {
    List<Klijent> findAll();
    Optional<Klijent> findById(Long id);
    Optional<Klijent> findByKorisnickoIme(String ime);
    Optional<Klijent> findByEPosta(String ePosta);
    void deleteById(Long id);
    int countByKorisnickoIme(String korisnickoIme);
    int countByEPosta(String ePosta);

}
