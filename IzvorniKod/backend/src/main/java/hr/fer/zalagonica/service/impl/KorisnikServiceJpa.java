package hr.fer.zalagonica.service.impl;

import hr.fer.zalagonica.dao.KorisnikRepository;
import hr.fer.zalagonica.domain.Korisnik;
import hr.fer.zalagonica.service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class KorisnikServiceJpa implements KorisnikService {

    @Autowired
    KorisnikRepository korisnikRepository;

    @Override
    public Optional<Korisnik> findByUsername(String username) {
        return korisnikRepository.findByKorisnickoIme(username);
    }
}
