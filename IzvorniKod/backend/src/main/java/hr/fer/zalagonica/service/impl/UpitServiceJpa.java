package hr.fer.zalagonica.service.impl;


import hr.fer.zalagonica.dao.UpitRepository;
import hr.fer.zalagonica.domain.Upit;
import hr.fer.zalagonica.service.UpitService;
import hr.fer.zalagonica.service.RequestDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

@Service
public class UpitServiceJpa implements UpitService {

    @Autowired
    private UpitRepository upitRepo;

    @Override
    public List<Upit> dohvatiUpite() {
        return upitRepo.findAll();
    }

    @Override
    public Optional<Upit> findById(Long id) {
        return upitRepo.findById(id);
    }

    @Override
    public Upit kreirajUpit(Upit upit) {
        Assert.notNull(upit, "Inquiry object must be given");
        Assert.isNull(upit.getId(), "Inquiry ID must be null, not " + upit.getId());
        Assert.hasText(upit.getMail(), "E-mail must not be null");
        Assert.hasText(upit.getTekstUpita(), "Mail body must not be null");
        Assert.hasText(upit.getNaslov(), "E-mail must not be null");

        return upitRepo.save(upit);

    }

    @Override
    public String izbrisiUpit(Long id) {
        Upit upit = upitRepo.findById(id).orElseThrow(() -> new RequestDeniedException("No inquiry with id " + id));
        upitRepo.deleteById(upit.getId());
        return "Inquiry '" + upit.getId() + "' has been succesfully removed from database.";
    }

    @Override
    public Upit azurirajUpit(Upit upit) {
        Assert.notNull(upit, "Inquiry object must be given");

        Long id = upit.getId();
        Upit noviUpit = upitRepo.findById(id).orElseThrow(() -> new RequestDeniedException(("No inquiry with id " + id)));

        noviUpit.setMail(upit.getMail());
        noviUpit.setNaslov(upit.getNaslov());
        noviUpit.setTekstUpita(upit.getTekstUpita());

        return upitRepo.save(noviUpit);
    }

    @Override
    public void odgovoriNaUpit(Long id, String tekstMaila) {
        try {
            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");

            Upit upit = upitRepo.findById(id).orElseThrow(() -> new RequestDeniedException(("No inquiry with id " + id)));


            Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication("zalagaonica.pawn@gmail.com", "pawnstars12!");
                }
            });
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress("zalagaonica.pawn@gmail.com", false));

            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(upit.getMail()));

            msg.setSubject("Odgovor na upit: " + upit.getNaslov());
            msg.setContent(tekstMaila, "text/html");
            msg.setSentDate(new Date());
            Transport.send(msg);
            izbrisiUpit(id);

        } catch (AddressException ae) {
            ae.printStackTrace();
        } catch (MessagingException me) {
            me.printStackTrace();
        }
    }


}
