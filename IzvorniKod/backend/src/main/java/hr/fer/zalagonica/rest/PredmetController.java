package hr.fer.zalagonica.rest;


import hr.fer.zalagonica.domain.Dug;
import hr.fer.zalagonica.domain.Klijent;
import hr.fer.zalagonica.domain.Predmet;
import hr.fer.zalagonica.service.KlijentService;
import hr.fer.zalagonica.service.PredmetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/predmeti")
@CrossOrigin(origins="http://localhost:3000")
public class PredmetController {

    @Autowired
    private PredmetService predmetService;

    @GetMapping("")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public List<Predmet> dohvatiPredmete() {
        return predmetService.dohvatiPredmete();
    }

    @GetMapping("ponuda")
    public List<Predmet> dohvatiPredmeteUPonudi() {
        return predmetService.dohvatiPredmeteUPonudi();
    }

    @GetMapping("posebnaPonuda")
    public List<Predmet> dohvatiPredmeteUPosebnojPonudi() {
        return predmetService.dohvatiPredmeteUPosebnojPonudi();
    }

    @PostMapping("kreiraj")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public Predmet kreirajPredmet(@RequestBody Predmet predmet) {
        return predmetService.kreirajPredmet(predmet);
    }

    @PostMapping("izbrisi")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public String izbrisiPredmet(@RequestBody long id) {
        return predmetService.izbrisiPredmet(id);
    }

    @PostMapping("azuriraj")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public Predmet azurirajPredmet(@RequestBody Predmet predmet) {
        return predmetService.azurirajPredmet(predmet);
    }

    @GetMapping("pretrazi/{kljucneRijeci}")
    public List<Predmet> pretraziPredmete(@PathVariable String kljucneRijeci){
        return predmetService.dohvatiPoKljucnimRijecima(kljucneRijeci);
    }

    @PostMapping("staviUPonudu")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public void dodajUPonudu(@RequestBody Predmet predmet){
        predmetService.staviUPonudu(predmet);
    }

    @PostMapping("staviUPosebnuPonudu")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public void dodajUPosebnuPonudu(@RequestBody Predmet predmet){
        predmetService.staviUPosebnuPonudu(predmet);
    }
    
    @GetMapping("predmet")
    public Predmet dohvatPredmete(@RequestParam long id, @RequestParam String username, @RequestParam String role) {
        return predmetService.dohvatiPredmet(id, username, role);
    }
    
    @PostMapping("predmetiKlijent")
    @Secured({"ROLE_KLIJENT"})
    public List<Predmet> dohvatPredmeteKlijent(@RequestBody Klijent	klijent) {
        return predmetService.dohvatiPredmeteKlijenta(klijent);
    }



}
