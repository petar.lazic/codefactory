package hr.fer.zalagonica.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
public class Transakcija {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private LocalDateTime datum;

    @NotNull
    private String vrstaTransakcije;

    @NotNull
    private Double iznos;

    @NotNull
    @OneToOne
    private Predmet predmet;

    @OneToOne
    private Djelatnik djelatnik;

    @OneToOne
    private Klijent klijent;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDatum() {
        return datum;
    }

    public void setDatum(LocalDateTime datum) {
        this.datum = datum;
    }

    public String getVrstaTransakcije() {
        return vrstaTransakcije;
    }

    public void setVrstaTransakcije(String vrstaTransakcije) {
        this.vrstaTransakcije = vrstaTransakcije;
    }

    public Predmet getPredmet() {
        return predmet;
    }

    public void setPredmet(Predmet predmet) {
        this.predmet = predmet;
    }

    public Djelatnik getDjelatnik() {
        return djelatnik;
    }

    public void setDjelatnik(Djelatnik djelatnik) {
        this.djelatnik = djelatnik;
    }

    public Klijent getKlijent() {
        return klijent;
    }

    public void setKlijent(Klijent klijent) {
        this.klijent = klijent;
    }

    public Double getIznos() {
        return iznos;
    }

    public void setIznos(Double iznos) {
        this.iznos = iznos;
    }
}
