package hr.fer.zalagonica.rest;

import com.fasterxml.jackson.databind.util.JSONPObject;
import hr.fer.zalagonica.service.KlijentService;
import hr.fer.zalagonica.service.RequestDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/auth")
@CrossOrigin(origins="http://localhost:3000")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    KlijentService klijentService;

    @PostMapping("login")
    public ResponseEntity<?> logirajKorisnika(HttpServletRequest req, @RequestBody LoginRequest request) {
        try {
            //System.out.println(req);
            Authentication auth = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
            SecurityContext securityContext = SecurityContextHolder.getContext();
            securityContext.setAuthentication(auth);

            HttpSession session = req.getSession(true);
            session.setAttribute("SPRING_SECURITY_CONTEXT", securityContext);
            System.out.println(session.getId());

            ModelMap model = new ModelMap();
            model.addAttribute("korisnickoIme", request.getUsername());
            model.addAttribute("lozinka", request.getPassword());
            Collection<?> coll = auth.getAuthorities();
            List<?> lista = (List) coll;
            model.addAttribute("uloga", lista.get(0).toString());

           return ResponseEntity.ok(model);
        } catch (AuthenticationException ex) {
            return new ResponseEntity<String>("False login",HttpStatus.UNAUTHORIZED);
        }

    }

    @PostMapping("register")
    public ResponseEntity<?> registrirajKorisnika(HttpServletRequest req, @RequestBody KlijentDTO klijentDTO) {
        System.out.println("Usao u registraciju");
        try {
            klijentService.kreirajKlijenta(klijentDTO);
        } catch (RequestDeniedException ex) {
            System.out.println(ex.getMessage());
            return new ResponseEntity<String>(ex.getMessage(),HttpStatus.UNAUTHORIZED);
        }

        System.out.println("Kreiran");
        LoginRequest request = new LoginRequest();
        request.setUsername(klijentDTO.getKorisnickoIme());
        request.setPassword(klijentDTO.getLozinka());
        return logirajKorisnika(req, request);
    }
}
