package hr.fer.zalagonica.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
public class Slika {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String put;

    public Long getId() {
        return id;
    }

    public String getPut() {
        return put;
    }

    public void setPut(String put) {
        this.put = put;
    }
}
