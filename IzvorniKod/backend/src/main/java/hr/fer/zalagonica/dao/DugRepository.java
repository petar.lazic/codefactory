package hr.fer.zalagonica.dao;

import hr.fer.zalagonica.domain.Dug;
import hr.fer.zalagonica.domain.Klijent;
import hr.fer.zalagonica.domain.Predmet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DugRepository extends JpaRepository<Dug, Long> {
    List<Dug> findAll();
    Optional<Dug> findById(Long id);
    void deleteById(Long id);
    Dug findByPredmet(Predmet predmet);
}
