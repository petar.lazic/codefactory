package hr.fer.zalagonica.rest;


import hr.fer.zalagonica.domain.Klijent;
import hr.fer.zalagonica.domain.Transakcija;
import hr.fer.zalagonica.service.KlijentService;
import hr.fer.zalagonica.service.RequestDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.User;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/klijenti")
@CrossOrigin(origins="http://localhost:3000")
public class KlijentController {

    @Autowired
    private KlijentService klijentService;

    @GetMapping("")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public List<Klijent> dohvatiKlijente() {
        return klijentService.dohvatiKlijente();
    }

    @Secured("ROLE_KLIJENT")
    @PostMapping("profil")
    public Klijent pogledajProfil(Principal p) {
        System.out.println("Usao " + p.getName());
        return klijentService.findByUsername(p.getName()).orElseThrow(()-> new RequestDeniedException("Ne postoji profil"));
    }

    @PostMapping("registriraj")
    public Klijent registiraj(@RequestBody @Valid KlijentDTO klijentDTO) {
        Klijent registered = new Klijent();
        return  klijentService.kreirajKlijenta(klijentDTO);
    }

    @PostMapping("izbrisi")
    @Secured("ROLE_ADMIN")
    public String izbrisiKlijenta(@RequestBody String username) {
       return klijentService.izbrisiKlijenta(username);
    }

    @PostMapping("azuriraj")
    @Secured({"ROLE_KLIJENT", "ROLE_ADMIN"})
    public ResponseEntity<?> azurirajKlijenta(@RequestBody KlijentDTO klijentDTO, Principal principal) {
        try {
            return ResponseEntity.ok(klijentService.azurirajKlijenta(klijentDTO, principal.getName()));
        } catch (Exception ex) {
            return new ResponseEntity<String>(ex.getMessage(), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("ugovor")
    @Secured("ROLE_KLIJENT")
    public ResponseEntity<byte[]> generirajUgovor(@RequestBody Transakcija transakcija){
        return klijentService.generirajUgovor(transakcija);
    }

}
