package hr.fer.zalagonica.rest;



import hr.fer.zalagonica.domain.Slika;
import hr.fer.zalagonica.service.SlikaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/slike")
@CrossOrigin(origins="http://localhost:3000")
public class SlikaController {

    @Autowired
    private SlikaService slikaService;

    @GetMapping("")
    @Secured("ROLE_ADMIN")
    public List<Slika> dohvatiSlike() {
        return slikaService.dohvatiSlike();
    }

    @PostMapping("registriraj")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public Slika kreirajSliku(@RequestBody Slika slika) {
        return slikaService.kreirajSliku(slika);
    }

    @PostMapping("izbrisi")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public String izbrisiSliku(@RequestBody Long id) {
        return slikaService.izbrisiSliku(id);
    }

    @PostMapping("azuriraj")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public Slika azurirajDug(@RequestBody Slika slika) {
        return slikaService.azurirajSliku(slika);
    }
}
