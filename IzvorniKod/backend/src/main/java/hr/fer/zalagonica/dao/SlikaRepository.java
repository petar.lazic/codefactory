package hr.fer.zalagonica.dao;

import hr.fer.zalagonica.domain.Predmet;
import hr.fer.zalagonica.domain.Slika;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SlikaRepository extends JpaRepository<Slika, Long> {
    List<Slika> findAll();
    Optional<Slika> findById(Long id);
    void deleteById(Long id);
}
