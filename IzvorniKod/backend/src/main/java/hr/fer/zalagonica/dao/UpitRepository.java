package hr.fer.zalagonica.dao;

import hr.fer.zalagonica.domain.Upit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UpitRepository extends JpaRepository<Upit,Long> {

    List<Upit> findAll();
    Optional<Upit> findById(Long id);
    void deleteById(Long id);
}
