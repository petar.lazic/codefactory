package hr.fer.zalagonica.service.impl;

import hr.fer.zalagonica.dao.KlijentRepository;
import hr.fer.zalagonica.dao.KorisnikRepository;
import hr.fer.zalagonica.domain.Klijent;
import hr.fer.zalagonica.domain.Transakcija;
import hr.fer.zalagonica.domain.pdf.PDFUgovor;
import hr.fer.zalagonica.rest.KlijentDTO;
import hr.fer.zalagonica.service.KlijentService;
import hr.fer.zalagonica.service.RequestDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@Service
public class KlijentServiceJpa implements KlijentService {

    @Autowired
    private KorisnikRepository korisnikRepo;

    @Autowired
    private KlijentRepository klijentRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public List<Klijent> dohvatiKlijente() {
        return klijentRepo.findAll();
    }

    @Override
    public Optional<Klijent> findById(Long id) {
        Assert.notNull(id, "ID must be given");
        return klijentRepo.findById(id);
    }

    @Override
    public Optional<Klijent> findByUsername(String username) {
        Assert.notNull(username, "ID must be given");
        return klijentRepo.findByKorisnickoIme(username);
    }

    @Override
    public Optional<Klijent> findByEmail(String email) {
        Assert.notNull(email, "ID must be given");
        return klijentRepo.findByEPosta(email);
    }

    @Override
    public Klijent kreirajKlijenta(KlijentDTO klijentDTO) {
        if(emailExists(klijentDTO.getePosta())) {
            throw new RequestDeniedException("Mail postoji.");
        }
        if(usernameExists(klijentDTO.getKorisnickoIme())) {
            throw new RequestDeniedException("Korisnicko ime postoji.");
        }
        if(!klijentDTO.getLozinka().equals(klijentDTO.getMatchingLozinka())) {
            throw new RequestDeniedException("Neispravna druga lozinka");
        }
        if(!klijentDTO.getIban().isEmpty() && klijentDTO.getIban().length() != 21) {
        	
            throw new RequestDeniedException("IBAN mora biti duljine 21 znak.");
        }
        if(klijentDTO.getIme().length() < 3 || klijentDTO.getIme().length()  > 45 || klijentDTO.getPrezime().length() < 3 || klijentDTO.getPrezime().length()  > 45) {
            throw new RequestDeniedException("Ime i prezime moraju biti u rasponu 3-45.");
        }

        Klijent klijent = new Klijent();
        klijent.setIme(klijentDTO.getIme());
        klijent.setPrezime(klijentDTO.getPrezime());
        klijent.setLozinka(passwordEncoder.encode(klijentDTO.getLozinka()));
        if(!klijentDTO.getIban().isEmpty()) klijent.setIban(klijentDTO.getIban());
        klijent.setePosta(klijentDTO.getePosta());
        klijent.setBrojUpita(0);
        if(!klijentDTO.getKontaktBroj().isEmpty()) klijent.setKontaktBroj(klijentDTO.getKontaktBroj());
        klijent.setKorisnickoIme(klijentDTO.getKorisnickoIme());

        return klijentRepo.save(klijent);
    }

    private boolean emailExists(String mail) {
        return korisnikRepo.countByEPosta(mail) > 0;
    }

    private boolean usernameExists(String username) {
        return korisnikRepo.countByKorisnickoIme(username) > 0;
    }

    @Override
    public String izbrisiKlijenta(String korisnickoIme) {
        Klijent klijent = klijentRepo.findByKorisnickoIme(korisnickoIme).orElseThrow(() -> new RequestDeniedException("No user with username " + korisnickoIme));
        klijentRepo.deleteById(klijent.getId());
        return "Client '" + korisnickoIme + "' has been succesfully removed from database.";
    }

    @Override
    public Klijent azurirajKlijenta(KlijentDTO klijentDTO, String korisnickoIme) {
        Klijent noviKlijent = klijentRepo.findByKorisnickoIme(korisnickoIme).orElseThrow(()->new RequestDeniedException(("No user with username " + korisnickoIme)));



        if(emailExists(klijentDTO.getePosta()) && !klijentDTO.getePosta().equals(noviKlijent.getePosta())) {
            throw new RequestDeniedException("Mail postoji.");
        }

        System.out.println(klijentDTO.getKorisnickoIme());
        if(usernameExists(klijentDTO.getKorisnickoIme()) && !klijentDTO.getKorisnickoIme().equals(korisnickoIme)) {
            throw new RequestDeniedException("Korisnicko ime postoji.");
        }


        noviKlijent.setIme(klijentDTO.getIme());
        noviKlijent.setPrezime(klijentDTO.getPrezime());
        noviKlijent.setePosta(klijentDTO.getePosta());
        noviKlijent.setIban(klijentDTO.getIban());
        noviKlijent.setKontaktBroj(klijentDTO.getKontaktBroj());
        noviKlijent.setKorisnickoIme(klijentDTO.getKorisnickoIme());
        return klijentRepo.save(noviKlijent);
    }

    @Override
    public ResponseEntity<byte[]> generirajUgovor(Transakcija transakcija) {
            PDFUgovor ugovor = new PDFUgovor();
            return ugovor.generirajPDF(transakcija);
    }
}
