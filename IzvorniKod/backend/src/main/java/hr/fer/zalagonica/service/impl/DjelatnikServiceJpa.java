package hr.fer.zalagonica.service.impl;

import hr.fer.zalagonica.dao.DjelatnikRepository;
import hr.fer.zalagonica.dao.KlijentRepository;
import hr.fer.zalagonica.dao.KorisnikRepository;
import hr.fer.zalagonica.dao.TransakcijaRepository;
import hr.fer.zalagonica.domain.Djelatnik;
import hr.fer.zalagonica.domain.Klijent;
import hr.fer.zalagonica.domain.Transakcija;
import hr.fer.zalagonica.domain.pdf.PDFStatistika;
import hr.fer.zalagonica.domain.pdf.PDFUgovor;
import hr.fer.zalagonica.rest.KlijentDTO;
import hr.fer.zalagonica.service.DjelatnikService;
import hr.fer.zalagonica.service.RequestDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@Service
public class DjelatnikServiceJpa implements DjelatnikService {

    @Autowired
    private KorisnikRepository korisnikRepo;

    @Autowired
    private DjelatnikRepository djelatnikRepo;
    
    @Autowired
    private KlijentRepository klijentRepo;


    @Autowired
    private TransakcijaRepository transakcijaRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public List<Djelatnik> dohvatiDjelatnike() {
        return djelatnikRepo.findAll();
    }

    @Override
    @Transactional
    public Djelatnik kreirajDjelatnika(Djelatnik djelatnik) {
        Assert.notNull(djelatnik, "Employee object must be given");
        Assert.isNull(djelatnik.getId(), "Employee ID must be null, not " + djelatnik.getId());

        String korisnickoIme = djelatnik.getKorisnickoIme();
        Assert.hasText(korisnickoIme, "Username must be given");

        if (usernameExists(korisnickoIme))
            throw new RequestDeniedException("User with username " + korisnickoIme + " already exists");

        if (emailExists(djelatnik.getIme()))
            throw new RequestDeniedException("Users with given e-mail already exists");

        djelatnik.setLozinka(passwordEncoder.encode(djelatnik.getLozinka()));
        return djelatnikRepo.save(djelatnik);
    }

    private boolean emailExists(String mail) {
        return korisnikRepo.countByEPosta(mail) > 0;
    }

    private boolean usernameExists(String username) {
        return korisnikRepo.countByKorisnickoIme(username) > 0;
    }


    @Override
    public String izbrisiDjelatnika(String korisnickoIme) {
        Djelatnik djelatnik = djelatnikRepo.findByKorisnickoIme(korisnickoIme).orElseThrow(() -> new RequestDeniedException("No user with username " + korisnickoIme));
        djelatnikRepo.deleteById(djelatnik.getId());
        return "Employee '" + korisnickoIme + "' has been succesfully removed from database.";
    }

    @Override
    public Djelatnik azurirajDjelatnika(Djelatnik djelatnik) {
        Assert.notNull(djelatnik, "Employee object must  be given");

        Long ID = djelatnik.getId();
        System.out.println("id je " + ID);
        Djelatnik noviDjelatnik = djelatnikRepo.findById(ID).orElseThrow(() -> new RequestDeniedException(("No user with username " + ID)));

        noviDjelatnik.setIme(djelatnik.getIme());
        noviDjelatnik.setPrezime(djelatnik.getPrezime());
        noviDjelatnik.setePosta(djelatnik.getePosta());
        noviDjelatnik.setKontaktBroj(djelatnik.getKontaktBroj());
        noviDjelatnik.setLozinka(passwordEncoder.encode(djelatnik.getLozinka()));
        noviDjelatnik.setKorisnickoIme(djelatnik.getKorisnickoIme());
        noviDjelatnik.setAdministrator(djelatnik.isAdministrator());


        return djelatnikRepo.save(noviDjelatnik);
    }

    @Override
    public ResponseEntity<byte[]> generirajUgovor(Long id) {
        PDFUgovor ugovor = new PDFUgovor();
        Transakcija transakcija = transakcijaRepo.findById(id).orElseThrow(() -> new RequestDeniedException(("No transaction with id " + id)));
        return ugovor.generirajPDF(transakcija);

    }

    @Override
    public ResponseEntity<byte[]> generirajStatistiku(int month, int year) {
        PDFStatistika statistika = new PDFStatistika();
        return statistika.generirajPDF(transakcijaRepo.getByYearAndMonth(month, year),month, year);
    }

    @Override
    public Optional<Djelatnik> findByUsername(String korisnickoIme) {
        return djelatnikRepo.findByKorisnickoIme(korisnickoIme);
    }
    
    @Override
    public Klijent kreirajKlijenta(KlijentDTO klijentDTO) {
        if(emailExists(klijentDTO.getePosta())) {
            throw new RequestDeniedException("Mail postoji.");
        }
        if(usernameExists(klijentDTO.getKorisnickoIme())) {
            throw new RequestDeniedException("Korisnicko ime postoji.");
        }
        if(!klijentDTO.getLozinka().equals(klijentDTO.getMatchingLozinka())) {
            throw new RequestDeniedException("Neispravna druga lozinka");
        }
        if(!klijentDTO.getIban().isEmpty() && klijentDTO.getIban().length() != 21) {
        	
            throw new RequestDeniedException("IBAN mora biti duljine 21 znak.");
        }
        if(klijentDTO.getIme().length() < 3 || klijentDTO.getIme().length()  > 45 || klijentDTO.getPrezime().length() < 3 || klijentDTO.getPrezime().length()  > 45) {
            throw new RequestDeniedException("Ime i prezime moraju biti u rasponu 3-45.");
        }

        Klijent klijent = new Klijent();
        klijent.setIme(klijentDTO.getIme());
        klijent.setPrezime(klijentDTO.getPrezime());
        klijent.setLozinka(passwordEncoder.encode(klijentDTO.getLozinka()));
        if(!klijentDTO.getIban().isEmpty()) klijent.setIban(klijentDTO.getIban());
        klijent.setePosta(klijentDTO.getePosta());
        klijent.setBrojUpita(0);
        if(!klijentDTO.getKontaktBroj().isEmpty()) klijent.setKontaktBroj(klijentDTO.getKontaktBroj());
        klijent.setKorisnickoIme(klijentDTO.getKorisnickoIme());

        return klijentRepo.save(klijent);
    }

}
