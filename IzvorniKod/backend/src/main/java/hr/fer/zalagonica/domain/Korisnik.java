package hr.fer.zalagonica.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="vrsta_korisnika")
public class Korisnik {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Size(min = 3, max = 45)
    private String ime;

    @NotNull
    @Size(min = 3, max = 45)
    private String prezime;

    @Size(min = 0, max = 45)
    private String kontaktBroj;

    @Column(unique = true, nullable = false)
    @Size(min = 3, max = 45)
    private String ePosta;

    @Column(unique = true, nullable = false)
    @Size(min = 3, max = 45)
    private String korisnickoIme;

    @NotNull
    @Size(min = 6)
    private String lozinka;

    public String getPrezime() {
        return prezime;
    }

    public String getIme() {
        return ime;
    }

    public String getKontaktBroj() {
        return kontaktBroj;
    }

    public String getePosta() {
        return ePosta;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public Long getId() {
        return id;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setePosta(String ePosta) {
        this.ePosta = ePosta;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public void setKontaktBroj(String kontaktBroj) {
        this.kontaktBroj = kontaktBroj;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }
}