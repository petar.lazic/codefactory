package hr.fer.zalagonica.service;

import hr.fer.zalagonica.domain.Djelatnik;
import hr.fer.zalagonica.domain.Klijent;
import hr.fer.zalagonica.domain.Transakcija;
import hr.fer.zalagonica.rest.KlijentDTO;

import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface DjelatnikService {
    List<Djelatnik> dohvatiDjelatnike();
    Djelatnik kreirajDjelatnika(Djelatnik djelatnik);
    String izbrisiDjelatnika(String korisnickoIme);
    Klijent kreirajKlijenta(KlijentDTO klijentDTO);
    public Djelatnik azurirajDjelatnika(Djelatnik djelatnik);
    ResponseEntity<byte[]> generirajUgovor(Long id);
    ResponseEntity<byte[]> generirajStatistiku(int month, int year);
    Optional<Djelatnik> findByUsername(String korisnickoIme);
}
