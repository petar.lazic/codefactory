package hr.fer.zalagonica.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
public class Upit {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String tekstUpita;

    @NotNull
    private String mail;

    @NotNull
    private String naslov;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTekstUpita() {
        return tekstUpita;
    }

    public void setTekstUpita(String tekstUpita) {
        this.tekstUpita = tekstUpita;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getNaslov() {
        return naslov;
    }

    public void setNaslov(String naslov) {
        this.naslov = naslov;
    }
}