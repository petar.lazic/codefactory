package hr.fer.zalagonica.service.impl;

import hr.fer.zalagonica.dao.DugRepository;
import hr.fer.zalagonica.dao.PredmetRepository;
import hr.fer.zalagonica.domain.Dug;
import hr.fer.zalagonica.domain.Klijent;
import hr.fer.zalagonica.domain.Predmet;
import hr.fer.zalagonica.service.DugService;
import hr.fer.zalagonica.service.RequestDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class DugServiceJpa implements DugService {

    @Autowired
    private DugRepository dugRepo;

    @Autowired
    private PredmetRepository predmetRepo;

    @Override
    public List<Dug> dohvatiDugove() {
        return dugRepo.findAll();
    }

    @Override
    public Optional<Dug> findById(Long id) {
        Assert.notNull(id, "ID must be given");
        return dugRepo.findById(id);
    }

    @Override
    @Transactional
    public Dug kreirajDug(Dug dug) {
        Assert.notNull(dug, "Debt object must be given");
        Assert.isNull(dug.getId(), "Debt ID must be null, not " + dug.getId());

        Double iznos = dug.getIznos();
        Assert.notNull(iznos, "Value can't be null");

        dug.setRokOtplate(LocalDateTime.now().plusMonths(3));
        dug.setDatumZaduzenja(LocalDateTime.now());

        return dugRepo.save(dug);
    }

    @Override
    public String izbrisiDug(Long id) {
        Dug dug = dugRepo.findById(id).orElseThrow(() -> new RequestDeniedException("No debt with id " + id));
        dugRepo.deleteById(dug.getId());
        return "Debt '" + id + "' has been succesfully removed from database.";
    }

    @Override
    public Dug azurirajDug(Dug dug) {
        Assert.notNull(dug, "Debt object must  be given");

        Long id = dug.getId();
        Dug noviDug = dugRepo.findById(id).orElseThrow(() -> new RequestDeniedException(("No debt with id " + id)));

        noviDug.setIznos(dug.getIznos());
        noviDug.setDatumZaduzenja(dug.getDatumZaduzenja());
        noviDug.setRokOtplate(dug.getRokOtplate());

        return dugRepo.save(noviDug);
    }

    @Override
    public Dug dohvatiDug(Long id) {
        Assert.notNull(id," Item ID must not be null");

        Predmet predmet = predmetRepo.findById(id).orElseThrow(() -> new RequestDeniedException(("No item with id " + id)));
        return dugRepo.findByPredmet(predmet);
    }
}
