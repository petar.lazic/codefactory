package hr.fer.zalagonica.domain;

import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class Predmet {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String imePredmeta;

    @NotNull
    private LocalDateTime datumUvrstenja;

    @NotNull
    private double cijena;

    @NotNull
    private boolean posebnaPonuda;

    @NotNull
    private boolean uPonudi;

    @OneToMany
    private List<Slika> slike;

    @OneToOne
    private Klijent klijent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImePredmeta() {
        return imePredmeta;
    }

    public void setImePredmeta(String imePredmeta) {
        this.imePredmeta = imePredmeta;
    }

    public LocalDateTime getDatumUvrstenja() {
        return datumUvrstenja;
    }

    public void setDatumUvrstenja(LocalDateTime datumUvrstenja) {
        this.datumUvrstenja = datumUvrstenja;
    }

    public double getCijena() {
        return cijena;
    }

    public void setCijena(double cijena) {
        this.cijena = cijena;
    }

    public boolean isPosebnaPonuda() {
        return posebnaPonuda;
    }

    public void setPosebnaPonuda(boolean posebnaPonuda) {
        this.posebnaPonuda = posebnaPonuda;
    }

    public List<Slika> getSlike() {
        return slike;
    }

    public void setSlike(List<Slika> slike) {
        this.slike = slike;
    }

    public Klijent getKlijent() {
        return klijent;
    }

    public void setKlijent(Klijent klijent) {
        this.klijent = klijent;
    }

    public boolean isuPonudi() {
        return uPonudi;
    }

    public void setuPonudi(boolean uPonudi) {
        this.uPonudi = uPonudi;
    }
}
