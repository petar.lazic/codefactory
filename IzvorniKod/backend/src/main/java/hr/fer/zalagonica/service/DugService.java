package hr.fer.zalagonica.service;

import hr.fer.zalagonica.domain.Dug;

import java.util.List;
import java.util.Optional;

public interface DugService {
    List<Dug> dohvatiDugove();
    Optional<Dug> findById(Long id);
    Dug kreirajDug(Dug dug);
    String izbrisiDug(Long id);
    Dug azurirajDug(Dug dug);
    Dug dohvatiDug(Long id);
}
