package hr.fer.zalagonica.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
public class Dug {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private double iznos;

    @NotNull
    private LocalDateTime datumZaduzenja;

    @NotNull
    private LocalDateTime rokOtplate;

    @NotNull
    @OneToOne
    private Predmet predmet;

    public Predmet getPredmet() {
        return predmet;
    }

    public void setPredmet(Predmet predmet) {
        this.predmet = predmet;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getIznos() {
        return iznos;
    }

    public void setIznos(double iznos) {
        this.iznos = iznos;
    }

    public LocalDateTime getDatumZaduzenja() {
        return datumZaduzenja;
    }

    public void setDatumZaduzenja(LocalDateTime datumZaduzenja) {
        this.datumZaduzenja = datumZaduzenja;
    }

    public LocalDateTime getRokOtplate() {
        return rokOtplate;
    }

    public void setRokOtplate(LocalDateTime rokOtplate) {
        this.rokOtplate = rokOtplate;
    }
}
