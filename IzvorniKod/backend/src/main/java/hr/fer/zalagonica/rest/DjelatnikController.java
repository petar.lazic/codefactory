package hr.fer.zalagonica.rest;


import hr.fer.zalagonica.domain.Djelatnik;
import hr.fer.zalagonica.domain.Klijent;
import hr.fer.zalagonica.domain.Transakcija;
import hr.fer.zalagonica.domain.pdf.PDFStatistika;
import hr.fer.zalagonica.service.DjelatnikService;
import hr.fer.zalagonica.service.KlijentService;
import hr.fer.zalagonica.service.RequestDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Calendar;
import java.util.List;

@RestController
@RequestMapping("/djelatnici")
@CrossOrigin(origins="http://localhost:3000")
public class DjelatnikController {

    @Autowired
    private DjelatnikService djelatnikService;
    
    @Autowired
    KlijentService klijentService;


    @GetMapping("")
    @Secured("ROLE_ADMIN")
    public List<Djelatnik> dohvatiDjelatnike() {
        return djelatnikService.dohvatiDjelatnike();
    }

    @PostMapping("profil")
    public Djelatnik pogledajProfil(@RequestBody String username) {
        System.out.println("Usao " + username);
        return djelatnikService.findByUsername(username).orElseThrow(()-> new RequestDeniedException("Ne postoji profil"));
    }

    @PostMapping("registriraj")
    @Secured("ROLE_ADMIN")
    public Djelatnik createDjelatnik(@RequestBody Djelatnik djelatnik) {
        return djelatnikService.kreirajDjelatnika(djelatnik);
    }
    
    @PostMapping("registrirajKlijenta")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> createKlijent(@RequestBody KlijentDTO klijentDTO) {
        System.out.println("Usao u registraciju");
        try {
            klijentService.kreirajKlijenta(klijentDTO);
        } catch (RequestDeniedException ex) {
            System.out.println(ex.getMessage());
            return new ResponseEntity<String>(ex.getMessage(),HttpStatus.UNAUTHORIZED);
        }
        System.out.println("Kreiran");
        return ResponseEntity.ok(null);

        
    }


    @PostMapping("izbrisi")
    @Secured("ROLE_ADMIN")
    public String izbrisiDjelatnika(@RequestBody String username) {
        return djelatnikService.izbrisiDjelatnika(username);
    }

    @PostMapping("azuriraj")
    @Secured("ROLE_ADMIN")
    public Djelatnik azurirajDjelatnika(@RequestBody Djelatnik djelatnik) {
        return djelatnikService.azurirajDjelatnika(djelatnik);
    }

    @GetMapping("ugovor/{id}")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public ResponseEntity<byte[]> generirajUgovor(@PathVariable Long id){
        return djelatnikService.generirajUgovor(id);
    }

    @GetMapping("statistika/{month}/{year}")
    @Secured({"ROLE_DJELATNIK", "ROLE_ADMIN"})
    public ResponseEntity<byte[]> generirajStatistiku(@PathVariable Integer month, @PathVariable Integer year){
        return djelatnikService.generirajStatistiku(month, year);
    }

}
