package hr.fer.zalagonica.dao;

import hr.fer.zalagonica.domain.Djelatnik;
import hr.fer.zalagonica.domain.Klijent;
import hr.fer.zalagonica.domain.Predmet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface PredmetRepository extends JpaRepository<Predmet, Long> {
    List<Predmet> findAll();
    Optional<Predmet> findById(Long id);
    List<Predmet> findByKlijent(Klijent klijent);
    Optional<Predmet> findByImePredmeta(String imePredmeta);
    Optional<Predmet> findByDatumUvrstenja(LocalDateTime datumUvrstenja);
    List<Predmet> findByUPonudi(Boolean ponuda);
    List<Predmet> findByPosebnaPonuda(Boolean posebnaPonuda);
    @Query("select p from Predmet p where p.imePredmeta like %?1%")
    List<Predmet> getSearch(String ime);
    void deleteById(Long id);
}
