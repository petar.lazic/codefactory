package hr.fer.zalagonica.service.impl;

import hr.fer.zalagonica.dao.PredmetRepository;
import hr.fer.zalagonica.domain.Klijent;
import hr.fer.zalagonica.domain.Predmet;
import hr.fer.zalagonica.service.PredmetService;
import hr.fer.zalagonica.service.RequestDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class PredmetServiceJpa implements PredmetService {


    @Autowired
    private PredmetRepository predmetRepo;

    @Override
    public List<Predmet> dohvatiPredmete() {
        return predmetRepo.findAll();
    }

    @Override
    public List<Predmet> dohvatiPredmeteUPonudi() {
        return predmetRepo.findByUPonudi(true);
    }

    @Override
    public List<Predmet> dohvatiPredmeteUPosebnojPonudi() {
        return predmetRepo.findByPosebnaPonuda(true);
    }

    @Override
    public void staviUPonudu(Predmet predmet) {
        Long id = predmet.getId();
        Predmet noviPredmet = predmetRepo.findById(id).orElseThrow(() -> new RequestDeniedException(("No item with id " + id)));
        noviPredmet.setuPonudi(true);
        predmetRepo.save(noviPredmet);
    }

    @Override
    public void staviUPosebnuPonudu(Predmet predmet) {
        Long id = predmet.getId();
        Predmet noviPredmet = predmetRepo.findById(id).orElseThrow(() -> new RequestDeniedException(("No item with id " + id)));
        noviPredmet.setPosebnaPonuda(true);
        predmetRepo.save(noviPredmet);
    }

    @Override
    public Predmet kreirajPredmet(Predmet predmet) {
        Assert.notNull(predmet, "Item object must be given");
        Assert.isNull(predmet.getId(), "Item ID must be null, not " + predmet.getId());

        String imePredmeta = predmet.getImePredmeta();
        Assert.hasText(imePredmeta, "Item name must be given");
        Assert.notNull(predmet.getCijena(), "Price must be given");
        if (predmet.getDatumUvrstenja() == null)
            predmet.setDatumUvrstenja(LocalDateTime.now());

        return predmetRepo.save(predmet);
    }

    @Override
    public String izbrisiPredmet(Long id) {
        Predmet predmet = predmetRepo.findById(id).orElseThrow(() -> new RequestDeniedException("No item with id " + id));
        predmetRepo.deleteById(predmet.getId());
        return "Item '" + predmet.getImePredmeta() + "' has been succesfully removed from database.";
    }


    @Override
    public Predmet azurirajPredmet(Predmet predmet) {
        Assert.notNull(predmet, "Item object must  be given");

        Long id = predmet.getId();
        Predmet noviPredmet = predmetRepo.findById(id).orElseThrow(() -> new RequestDeniedException(("No item with id " + id)));

        noviPredmet.setImePredmeta(predmet.getImePredmeta());
        noviPredmet.setCijena(predmet.getCijena());
        noviPredmet.setDatumUvrstenja(predmet.getDatumUvrstenja());
        noviPredmet.setKlijent(predmet.getKlijent());
        noviPredmet.setPosebnaPonuda(predmet.isPosebnaPonuda());
        noviPredmet.setSlike(predmet.getSlike());
        noviPredmet.setuPonudi(predmet.isuPonudi());
        noviPredmet.setPosebnaPonuda(predmet.isPosebnaPonuda());

        return predmetRepo.save(noviPredmet);
    }

    @Override
    public List<Predmet> dohvatiPoKljucnimRijecima(String trazeneRijeci) {
        List<Predmet> predmeti = predmetRepo.getSearch(trazeneRijeci);
        if (predmeti.isEmpty())
            return predmetRepo.findByUPonudi(true);
        else
            return predmeti;
    }

    @Override
    public Predmet dohvatiPredmet(Long id, String username, String role) {
        Predmet predmet = predmetRepo.findById(id).orElseThrow(() -> new RequestDeniedException("No item with id " + id));
        if (predmet.isuPonudi() || predmet.getKlijent().getKorisnickoIme().equals(username) || role.equals("ROLE_ADMIN") || role.equals("ROLE_DJELATNIK"))
            return predmet;
        else {
            throw new RequestDeniedException("No item with id " + id);
        }

    }

    @Override
    public List<Predmet> dohvatiPredmeteKlijenta(Klijent klijent) {
        return predmetRepo.findByKlijent(klijent);

    }


}
