package hr.fer.zalagonica.service;

import hr.fer.zalagonica.domain.Dug;
import hr.fer.zalagonica.domain.Slika;

import java.util.List;
import java.util.Optional;

public interface SlikaService {
    List<Slika> dohvatiSlike();
    Optional<Slika> findById(Long id);
    Slika kreirajSliku(Slika slika);
    String izbrisiSliku(Long id);
    Slika azurirajSliku(Slika slika);
}
