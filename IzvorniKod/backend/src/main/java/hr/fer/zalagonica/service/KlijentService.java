package hr.fer.zalagonica.service;

import hr.fer.zalagonica.domain.Klijent;
import hr.fer.zalagonica.domain.Transakcija;
import hr.fer.zalagonica.rest.KlijentDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface KlijentService {
    List<Klijent> dohvatiKlijente();
    Optional<Klijent> findById(Long id);
    Optional<Klijent> findByUsername(String username);
    Optional<Klijent> findByEmail(String ePosta);
    Klijent kreirajKlijenta(KlijentDTO klijentDTO);
    String izbrisiKlijenta(String korisnickoIme);
    Klijent azurirajKlijenta(KlijentDTO klijentDTO, String username);
    ResponseEntity<byte[]> generirajUgovor(Transakcija transakcija);
}
