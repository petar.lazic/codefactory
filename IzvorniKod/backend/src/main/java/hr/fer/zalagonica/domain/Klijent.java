package hr.fer.zalagonica.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@DiscriminatorValue("klijent")
public class Klijent extends Korisnik{

    @NotNull
    private int brojUpita;

    @Size(min=21, max=21)
    private String iban;


    public int getBrojUpita() {
        return brojUpita;
    }

    public String getIban() {
        return iban;
    }

    public void setBrojUpita(int brojUpita) {
        this.brojUpita = brojUpita;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }
}

