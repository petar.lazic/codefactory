import React, {Component} from "react"
import { withRouter } from "react-router-dom";
import Select from 'react-select'
import './style.css'
//import PDF from 'react-pdf-scroll'


class Statistika extends React.Component {
    constructor() {
        super()
        this.state = {
            mjesec: null,
			godina: null
        }
    }

    componentDidMount(){
    }

	handleChange(event){
		console.log("option selected");
        this.setState(
            {[event.target.name] : event.target.value}
            );
    }

	onSubmit = (e) => {
	  e.preventDefault();
	  console.log(this.state.mjesec + "/" + this.state.godina);
	  const options = {
		method: 'GET',
		headers: {
            //'Content-Type' : 'application/json',
            'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
           
		},
		responseType: 'application/pdf'
	  };
	  var win = window.open("http://localhost:8080/djelatnici/statistika/" + this.state.mjesec + "/" + this.state.godina, "blank_");
	  win.focus();		

	} 
	
    render(){
      //console.log("RENDER");
	  const godine = [{label:"2017", value:"2017"}, {label:"2018", value:"2018"}, {label:"2019", value:"2019"}]
      const mjeseci = [{label:"Siječanj", value:"1"}, {label:"Veljača", value:"2"}, {label:"Ožujak", value:"3"},
								{label:"Travanj", value:"4"}, {label:"Svibanj", value:"5"}, {label:"Lipanj", value:"6"},
								{label:"Srpanj", value:"7"}, {label:"Kolovoz", value:"8"}, {label:"Rujan", value:"9"},
								{label:"Listopad", value:"10"}, {label:"Studeni", value:"11"}, {label:"Prosinac", value:"12"}]

      return(
          <div className="opis">
              <h3>Statistika</h3>
			  <p> Odaberite razdoblje za koje želite generirati statistiku.</p>
              <div className="opisPredmeta">
                  <p><strong>Godina</strong></p>
				  <Select onChange={(s)=> this.setState({godina : s.value})}  options={godine}/>
				  <p><strong>Mjesec</strong></p>
				  <Select onChange={(s)=> this.setState({mjesec : s.value})}  options={mjeseci}/>
				  <p></p>
				  <button type='submit' disabled={!this.state.mjesec || !this.state.godina} id="submitButton" onClick={this.onSubmit}>Generiraj statistiku</button>
              </div>

          </div>
      )
  }
} 

export default withRouter(Statistika)