import React from 'react';
import { withRouter } from "react-router-dom";


import './App.css'
import './UPonudi.css'
import './UPonudi.scss'



import Card from './Card.js'


class UPonudi extends React.Component {
    constructor(props){
        super(props);
        this.state={
            predmeti: [],
            loaded: false,
            currentIndex: 0
			
        }



        this.nextProperty = this.nextProperty.bind(this);
        this.prevProperty = this.prevProperty.bind(this);
    }
    nextProperty(){
        const properties = this.state.properties;
        this.setState( prevState => ({
                currentIndex: prevState.currentIndex + 1
        }));
    }

    prevProperty(){
        this.setState( prevState => ({
            currentIndex: prevState.currentIndex - 1
    }));
    }

    getIndex(predmet){
        this.state.predmeti.findIndex( p => {return (predmet === p)});
    }
    componentDidMount(){
        this.dohvatiPredmete()
      }
	 
    dohvatiPredmete = () => {
        var link = 'http://localhost:8080/predmeti/'
        link += (this.props.match.params.search == undefined)?"ponuda":"pretrazi/" + this.props.match.params.search;
        console.log(this.props.match.params.search)
        console.log(link)
        fetch(link)
        .then((response) => response.json()).then(response => {

          this.setState({
              predmeti: response,
              loaded: true
          })
      });
      }

    render(){

        if (!this.state.loaded) return <h1> loading... </h1>
        const prop = {
            img: '',
            index: ''
        }
        const { predmeti, currentIndex} = this.state;
		

        return(
            <div className="UPonudi">
                <div className="divNaslovUPonudi"><h2 className="tekst">PONUDA</h2></div>

                <div  className="col">
                 <div className={`cards-slider active-slide-${currentIndex}`}>
                     <div className="cards-slider-wrapper"
                          style={{'transform': `translateX(-${currentIndex*(100/predmeti.length)}%)` }}>
                    {
                        predmeti.map((p, i) => (
                            <Card key={i} id={p.id} image={"http://localhost:8080/files/files/" + p.slike[0].put}  index={this.getIndex(p)} naziv={p.imePredmeta} cijena={p.cijena} />
                        ))

                    }
                    </div>
                 </div>
                </div>
				<div className="nazivcijena">
					Naziv: {(this.state.predmeti==undefined || this.state.predmeti[this.state.currentIndex]==undefined)?"-": this.state.predmeti[this.state.currentIndex].imePredmeta}, Cijena: {(this.state.predmeti==undefined || this.state.predmeti[this.state.currentIndex]==undefined)?"-": this.state.predmeti[this.state.currentIndex].cijena} HRK
				</div>
                <div className="buttonsDiv">
                    <button onClick={this.prevProperty} disabled={this.state.currentIndex === 0} className="buttonPonuda">Prethodni</button>
					<button onClick={()=> {this.props.history.push("/predmeti/" + predmeti[this.state.currentIndex].id)}} className="buttonPonuda">Detalji</button>
					<button onClick={this.nextProperty} disabled={this.state.currentIndex === this.state.predmeti.length - 1} className="buttonPonuda">Sljedeći</button>
                </div>
             </div>
        );
    }

};
export default withRouter(UPonudi);
