import React from "react"

function Footer(){
    return(
        <footer>
            <hr />
			<br/> <br/>
            <p><strong>Kontakt broj: </strong>01/ 383 5529</p>
            <p><strong>Adresa e-pošte: </strong>zalagaonica@gmail.com</p>
            <p><strong>Adresa: </strong>Tina Ujevića 20, 10 000 Zagreb</p>
        </footer>
    )
};

export default Footer