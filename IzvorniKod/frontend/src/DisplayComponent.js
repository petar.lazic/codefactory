import { Route} from 'react-router-dom';
import React, { Component } from 'react';

import SviPredmeti from "./SviPredmeti"
import Predmeti from "./Predmeti"
import Predmet from "./Predmet"
import Main from "./Main"
import PregledProfila from './PregledProfila';
import DodavanjePredmeta from './DodavanjePredmeta';
import IzmjenaPredmeta from './IzmjenaPredmeta';
import BrzaProcjena from './BrzaProcjena';
import UnosTransakcije from './UnosTransakcije';
import Registriraj from './Registriraj';
import PregledKorisnika from './PregledKorisnika';
import IzmjenaProfila from './IzmjenaProfila';
import Upit from './Upit';
import Upiti from './Upiti';
import Odgovor from './Odgovor';
import Statistika from './Statistika';



function DisplayComponent() {
    return (
        <div>
            <Route exact path = "/profil" component={PregledProfila} />
			<Route exact path="/statistika" component={Statistika} />
			<Route exact path="/" component={Main} />
            <Route exact path="/search/:search" component={Main} />
            <Route exact path="/predmetiPonuda" component={Predmeti} />
            <Route path="/predmeti/:predmetID" component={Predmet} />
            <Route exact path="/dodajPredmet" component={DodavanjePredmeta} />
            <Route exact path="/sviPredmeti" component={SviPredmeti} />
            <Route exact path="/izmjenaPredmeta/:predmetID" component={IzmjenaPredmeta} />
            <Route exact path="/unosTransakcije" component={UnosTransakcije} />
            <Route exact path="/unosTransakcije/:predmetID" component = {UnosTransakcije} />
            <Route exact path="/registriraj" component={Registriraj} />
            <Route exact path="/pregledKorisnika" component={PregledKorisnika} />
            <Route exact path="/izmjenaProfila/:tip/:username" component={IzmjenaProfila} />
            <Route exact path="/upit" component={Upit} />
            <Route exact path="/upiti" component={Upiti} />
            <Route exact path="/upiti/odgovori/:upitID" component={Odgovor} />
        </div>

    )
}

export default DisplayComponent
