import React from "react"
import { withRouter } from "react-router-dom";


class Odgovor extends React.Component {

    constructor(props) {
        super()
        this.state = {
            upit: {},
            id: props.match.params.upitID,
            available: false,
            odgovor:"",
            email:"",
            naslov: "",
            tekstUpita:"",
            errorodgovor: "odgovor prekratak, najmanje 10 znakova"
        }
    }

    componentDidMount() {
        console.log("mounted");
        const role = sessionStorage.getItem('uloga');
        const username = sessionStorage.getItem('korisnik');

        fetch("http://localhost:8080/upiti/jedan/"+this.state.id,{
            method: 'GET',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
            }})
        .then((response) => {
            console.log("status " + response.status)
            if (response.status >= 400) {
                console.log("status " + response.status)
                this.setState({
                    available: false
                })
            }
            return response.json()}).then(response => {
          console.log("rr:" + response.id);
            if (typeof response.id != "undefined") {
                console.log(response)
                this.setState({
                    upit: response,
                    email: response.mail,
                    naslov: response.naslov,
                    tekstUpita: response.tekstUpita,
                    available: true
                })

            }
      });
      
    }

    handleChange = (event) => {
        const {name, value, type, checked} = event.target
        type === "checkbox" ?
            this.setState({
                [name]: checked
            })
        :
        this.setState({
            [name]: value
        })

        
        if (name == "odgovor") {
            if (value.length < 21) this.setState({["error"+name]: "odgovor prekratak, najmanje 20 znakova"})
            else this.setState({["error"+name]: ""}) 
        }
         
    }


    posaljiOdgovor = () => {
        fetch("http://localhost:8080/upiti/odgovori/?id="+this.state.id+"&tekstMaila="+this.state.odgovor,{
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
            }})
        .then((response) => {
            console.log("status " + response.status)
            if (response.status >= 400) {
                console.log("status " + response.status)
                }
            else this.props.history.push("/upiti");
            })
            
    }

    render() {
        if (typeof this.state.upit == "undefined" || !this.state.available) {
            return (
                <div>
                    <h3> NOT FOUND </h3>
                </div>
            )
        }
        const disabled =   this.state.errorodgovor.toString().length > 0
        return (
            <div>
                <h2> poslao: {this.state.email} </h2>
                <h2> naslov: {this.state.naslov} </h2>
                <h3> tekst upita: {this.state.tekstUpita} </h3>
               
                <form>
                    <br />
                    <textarea name="odgovor" value={this.state.opis}
                        onChange={this.handleChange}
                        placeholder="odgovor" cols="150" rows="5"></textarea>


                </form>
                <h3 style={{color:"crimson", marginLeft:"10px", fontSize: "17px"}}> {this.state.errorodgovor} </h3>
                    
                <button  disabled={disabled}  onClick={this.posaljiOdgovor}> Posalji </button>
            </div>
        );

            


    }
}

export default withRouter(Odgovor)
