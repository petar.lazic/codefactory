import React from "react"
import ReactDOM from "react-dom"

import './Modal.css'

class Registracija extends React.Component{

    state = {
           ime : '',
           prezime : '',
		   kontaktBroj: '',
           email : '',
           korisnickoIme : '',
		   iban: '',
           lozinka : '',
           lozinka2 : '',
		   error: ''
    };


 handleChange = (e) => {
   this.setState({
     [e.target.name] : e.target.value
   });
 };

onSubmit = (e) => {
  e.preventDefault();
  const data = {
    ime: this.state.ime,
    prezime: this.state.prezime,
	kontaktBroj: this.state.kontaktBroj,
    ePosta: this.state.email,
    korisnickoIme: this.state.korisnickoIme,
	iban: this.state.iban,
    lozinka: this.state.lozinka,
    matchingLozinka: this.state.lozinka2
  };
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  };
  //console.log(JSON.stringify(data));
  fetch('http://localhost:8080/auth/register', options)
			.then((response) => {
                if (response.status === 401) {
                    this.setState( { error : "Neispravni podatci"});
                }
                return response.json()
            })
			.then((responseJson) => {
                this.props.onLogin(responseJson.korisnickoIme, responseJson.uloga, responseJson.lozinka);
                
            });
 }

 render(){
       return(
           <div className = 'Registracija'>
               <h1 className="naslovRegistracija">REGISTRACIJA</h1>
               <form onSubmit = {this.onSubmit} className = "Form">
                 <p> <input type = "text" name = "ime" placeholder = "Ime"   onChange = {this.handleChange} value = {this.state.ime} pattern="[a-zA-Z0-9]{3}[a-z0-9]*"/> </p>
                 <p> <input type = "text" name = "prezime" placeholder = "Prezime" onChange = {this.handleChange} value = {this.state.prezime} pattern="[a-zA-Z0-9]{3}[a-z0-9]*" /></p>
                 <p> <input type = "text" name = "kontaktBroj" placeholder = "Kontakt" onChange = {this.handleChange} value = {this.state.kontaktBroj} pattern="[0-9]{3}[0-9]*" /></p>
				 <p> <input type = "email" name = "email" placeholder = "Adresa e-pošte"  onChange = {this.handleChange} value = {this.state.email} pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"/></p>
                 <p> <input type = "text"  name = "korisnickoIme" placeholder = "Korisničko ime" onChange = {this.handleChange} value = {this.state.korisnicnoIme} pattern="[a-zA-Z0-9]{6}[a-z0-9]*"/></p>
                 <p> <input type = "text"  name = "iban" placeholder = "IBAN" onChange = {this.handleChange} value = {this.state.iban} pattern="^HR[0-9]{19}"/></p>
				<p> <input type = "password" name = "lozinka" placeholder = "Lozinka" onChange = {this.handleChange} value = {this.state.lozinka} pattern="(.){6}(.)*"/></p>
                 <p> <input type = "password" name = "lozinka2" placeholder = "Ponovno upišite lozinku" onChange = {this.handleChange} value = {this.state.lozinka2} pattern="(.){6}(.)*" /></p>
                 <p><input type = "text" value={this.state.error} style={{color:"crimson", marginLeft:"10px", fontSize: "15px"}} className="errorWindow" /></p>
				 <button type = "submit">REGISTRIRAJ SE</button>
               </form>
           </div>
       );

 }
}

export default Registracija
