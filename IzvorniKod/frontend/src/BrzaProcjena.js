import React from 'react'


import { FilePond, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css';
registerPlugin(FilePondPluginImagePreview);

class BrzaProcjena extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            files: []
        }
        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }


 handleChange(event){
    this.setState({
      [event.target.name] : event.target.value
    });
  };

  handleClick(event){
        document.getElementsByClassName("podatciProfila")[0].setAttribute("style", "pointer-events: auto")
        const inputs = document.getElementsByClassName("editable")
        for (var x = 0; x < inputs.length; x++) {
            inputs[x].setAttribute("style", "border: 1px solid gray");
        }
        console.log("aaa")
        this.setState({ disabled: "false" });
        const submitButton = document.getElementsByClassName("submitButton");
        submitButton[0].setAttribute("style", "border: 1px solid lightgray");
        submitButton[0].setAttribute("style", "color: black");
    }



    render(){
        return(
            <div className="opis">
                <h3>Obrazac za brzu procjenu</h3>
                <div className="opisPredmeta">
                    <p><strong>Naziv</strong><input type="text" placeholder = ". . . . . ." name="naziv" class="form-control" className="editable" value={this.state.naziv} onChange={this.handleChange}/></p><hr />
                    <p><strong>Opis</strong><input type="text" name="opis" placeholder = ". . . . . ." class="form-control"   className="editable" value={this.state.opis} onChange={this.handleChange}/></p><hr />
                    <FilePond allowMultiple={true} data-max-files="5"
                                 onupdatefiles={(fileItems) => {
                                   this.setState({
                                       files: fileItems.map(fileItem => fileItem.file)
                                   });
                                   console.log(this.state.files);

                               }}/>

                    <p><input type="submit" value="Spremi promjene" className="submitButton" color="transparent"/></p>
                </div>

            </div>
        )
    }
}

export default BrzaProcjena;
