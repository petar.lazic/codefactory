import React, {Component} from "react"
import { withRouter } from "react-router-dom";
import Select from 'react-select'

class Registriraj extends React.Component {
    constructor() {
        super()
        this.state = {
            ime : '', 
            prezime : '',
            kontaktBroj: '',
            email : '',
            korisnickoIme : '',
            iban: '',
            lozinka : '',
            lozinka2 : '',
            errorlozinka2: "* Lozinka prekratka, najmanje 5 znakova",
            erroremail: "* Adresa E-pošte nije valjana",
            errorkorisnickoIme: "* Korisnicko ime prekratko, najmanje 4 znaka",
            errorime: "* Ime prekratko, najmanje 2 znaka",
            errorprezime: "* Prezime prekratko, najmanje 2 znaka",
            tip:"",
            klijenti:[],
            djelatnici:[],
            admin: false
        }
    }

    componentDidMount(){
        const role = sessionStorage.getItem('uloga');
        const admin = role === "ROLE_ADMIN"

        if(admin) {
            fetch('http://localhost:8080/klijenti', {
            method: 'GET',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
            }})
        .then((response) => response.json()).then(response => {
          
          this.setState({
              klijenti: response
          })
        });
      fetch('http://localhost:8080/djelatnici', {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
        }})
        .then((response) => response.json()).then(response => {
        
        this.setState({
            djelatnici: response
            })
        });
        }

        
    }

    handleClick = () => {
        const data = {
            ime: this.state.ime,
            prezime: this.state.prezime,
            kontaktBroj: this.state.kontaktBroj,
            ePosta: this.state.email,
            korisnickoIme: this.state.korisnickoIme,
            iban: this.state.iban,
            lozinka: this.state.lozinka,
            matchingLozinka: this.state.lozinka2,
            administrator: this.state.admin
          };
          const options = {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
            },
            body: JSON.stringify(data)
          };
          const url = "http://localhost:8080/djelatnici/" + (this.state.tip === "djelatnik" ? "registriraj" : "registrirajKlijenta")
          
          fetch(url, options)
            .then(response => {
                console.log("code " + response.statusCode)
            })
		setTimeout(() => window.location.reload(), 500)	
    }

    validateEmail = (email) => {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    handleChange = (event) => {const {name, value, type, checked} = event.target
        type === "checkbox" ? 
            this.setState({
                [name]: checked
            })
        :
        this.setState({
            [name]: value
        })
        if (this.state.tip === "klijent" && this.state.admin === true) this.setState({admin:false})
        
        const korisnici = this.state.djelatnici.map(d => d.korisnickoIme).concat(this.state.klijenti.map(k => k.korisnickoIme))
        const emails = this.state.djelatnici.map(d => d.ePosta).concat(this.state.klijenti.map(k => k.ePosta))
            
        switch(name) {
            case "lozinka":
                if (value.length < 5  && this.state.lozinka2.length < 5) this.setState({["errorlozinka2"]: "* Lozinka prekratka, najmanje 5 znakova"})
                else if ((value.length > 4 || this.state.lozinka2.length > 4) && value != this.state.lozinka2) this.setState({["errorlozinka2"]: "lozinke se ne podudaraju"})
                else this.setState({["errorlozinka2"]: ""}) 
                break;
            case "lozinka2": 
                if (value.length < 5  && this.state.lozinka.length < 5) this.setState({["errorlozinka2"]: "* Lozinka prekratka, najmanje 5 znakova"})
                else if ((value.length > 4 || this.state.lozinka.length > 4) && value != this.state.lozinka ) this.setState({["errorlozinka2"]: "lozinke se ne podudaraju"})
                else this.setState({["errorlozinka2"]: ""}) 
                break;
            case "email":
                if (!this.validateEmail(value) && !emails.includes(value)) this.setState({["error"+name]: "* Adresa E-pošte nije valjana"})
                else if (emails.includes(value) && this.validateEmail(value)) this.setState({["error"+name]: "* Adresa E-pošte zauzeta"})
                else this.setState({["error"+name]: ""})
                break;
             
            case "korisnickoIme":
                if (value.length < 4 && !korisnici.includes(value)) this.setState({["error"+name]: "* Korisnicko ime prekratko, najmanje 4 znaka"})
                else if (value.length > 3 && korisnici.includes(value)) this.setState({["error"+name]: "* Korisnicko ime zauzeto"})
                else this.setState({["error"+name]: ""})
                break;
            case "ime":
                if (value.length < 3) this.setState({["error"+name]: "ime prekratko, najmanje 3 znaka"})
                else this.setState({["error"+name]: ""})    
                break;
            case "prezime":
                if (value.length < 2) this.setState({["error"+name]: "prezime prekratko, najmanje 2 znaka"})
                else this.setState({["error"+name]: ""})    
                break;
            
        }
    }



    render() {
        const role = sessionStorage.getItem('uloga');
        const admin = role === "ROLE_ADMIN"
        if (!admin) {
            return (
                <div> 
                    <h3> NOT FOUND </h3>
                </div>
            )
        }
        
        const disabled =    this.state.erroremail.toString().length > 0 || 
                            this.state.errorkorisnickoIme.toString().length > 0 || 
                            this.state.errorlozinka2.toString().length > 0 ||
                            this.state.errorime.toString().length > 0 ||
                            this.state.errorprezime.toString().length > 0
        
        
        return (
            <div>
               
                <form 
                        marginRight="10px" marginLeft="20px">
						<p><strong>Ime</strong></p>
                    <input 
                        name="ime" 
                        value={this.state.ime} 
                        onChange={this.handleChange} 
                        placeholder="Ime" 
						pattern="[a-zA-Z0-9]{3}[a-z0-9]*"
                    />
                    <p style={{color:"crimson", marginLeft:"10px", fontSize: "15px"}}> {this.state.errorime} </p>
          
                    <p><strong>Prezime</strong></p>
                    <input 
                        name="prezime" 
                        value={this.state.prezime}
                        onChange={this.handleChange} 
                        placeholder="Prezime" 
						pattern="[a-zA-Z0-9]{3}[a-z0-9]*"
                    />
                    <p style={{color:"crimson", marginLeft:"10px", fontSize: "15px"}}> {this.state.errorprezime} </p>
                   
					<p><strong>Elektronička pošta</strong> </p>
                    <input
                        name="email" 
                        value={this.state.email}
                        onChange={this.handleChange} 
                        placeholder="e-mail"
						pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                    /> 
                    <br />
					<p style={{color:"crimson", marginLeft:"10px", fontSize: "15px"}}> {this.state.erroremail} </p>
					
                    <p><strong>Korisničko ime</strong></p>
                    <input
                        name="korisnickoIme" 
                        value={this.state.korisnickoIme}
                        onChange={this.handleChange} 
                        placeholder="Korisnicko Ime"
						pattern="[a-zA-Z0-9]{6}[a-z0-9]*"
                    /> 
                    <br />
					<p style={{color:"crimson", marginLeft:"10px", fontSize: "15px"}}> {this.state.errorkorisnickoIme} </p>

					<p><strong>Lozinka</strong></p>
                    <input 
                        name="lozinka" 
                        value={this.state.lozinka}
                        onChange={this.handleChange} 
                        placeholder="lozinka"
                        type = "password"
						pattern="(.){6}(.)*"
                    />
                    <br />
                    <p style={{color:"crimson", marginLeft:"10px", fontSize: "15px"}}> {this.state.errorlozinka2} </p>

					<p><strong>Ponovljena lozinka</strong></p>
                    <input 
                        name="lozinka2" 
                        value={this.state.lozinka2}
                        onChange={this.handleChange} 
                        placeholder="ponovi lozinku"
                        type = "password"
						pattern="(.){6}(.)*"
                    />
                    <br />
                    <p style={{color:"crimson", marginLeft:"10px", fontSize: "15px"}}> {this.state.errorlozinka2} </p>

					<p><strong>Kontakt broj</strong></p>
                    <input
                        name="kontaktBroj" 
                        value={this.state.kontaktBroj}
                        onChange={this.handleChange} 
                        placeholder="Kontakt Broj"
						pattern="[0-9]{3}[0-9]*"
                    /> 
                    <br />
                    
					<p><strong>IBAN</strong></p>
                    <input 
                        name="iban" 
                        value={this.state.iban}
                        onChange={this.handleChange} 
                        placeholder="iban" 
						pattern="^\s*$|^HR[0-9]{19}"						
                    />
                    <br />
					
					<p><strong>Uloga osobe</strong></p>
                    <label>
                        <input 
                            type="radio" 
                            name="tip"
                            value=" klijent"
                            checked={this.state.tip === " klijent"}
                            onChange={this.handleChange}
                        /> klijent
                    </label>
                    
                    <label>
                        <input 
                            type="radio" 
                            name="tip"
                            value="djelatnik"
                            checked={this.state.tip === " djelatnik"}
                            onChange={this.handleChange}
                        /> djelatnik
                    </label>
                    

                    
                    <br />
                    {this.state.tip == "djelatnik" ? <label>
                        <input 
                            type="checkbox"
                            name="admin"
                            onChange={this.handleChange}
                            checked={this.state.admin}
                        /> Admin?
                    </label> : ""}
                    <br/>
                    
                    
                </form>
                
                <button disabled={disabled} onClick={this.handleClick}> Registriraj </button>
            </div>
        );
    }
} 

export default withRouter(Registriraj)