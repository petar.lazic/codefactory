import React, { Component } from 'react';
import logo from './logo.svg';

import './App.css';



import SpecijalnaPonuda from './SpecijalnaPonuda';
import UPonudi from './UPonudi';




class Main extends React.Component {
  constructor(){
    super();
    this.state = {
      search: ""
    }
  }

  pretrazi = (event) => {
    console.log(this.state.search)
    this.props.history.push("/search/" + this.state.search);
    window.location.reload()
  }

  handleChange = (event) => {
     this.setState({
       [event.target.name] : event.target.value
     });
   };

  render() {

    return(
      <div className="Main">
      
		<SpecijalnaPonuda />
	  
      <div>
		  <br/><br/>
		  <p><strong>Pretraži predmete u ponudi...</strong></p>
        <div className="searchBarDiv">
            <input type="text" name="search" className="searchBar" placeholder="Search..." onChange = {this.handleChange}/>
            <button className="searchButton" onClick={this.pretrazi}>Unesi</button>
          </div>
			<br/><br/>
      </div>
          
		  <UPonudi search = {this.props.match.params.search}/>
      </div>
    )
  }
}

export default Main;
