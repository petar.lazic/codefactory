import React, { Component } from "react";

import PredmetContainer from "./PredmetContainer"
import { withRouter } from "react-router-dom";

class Upiti extends Component {
    constructor() {
        super()
        this.state = {
          upiti: [],
          loading: false
        }
      }
      componentDidMount(){
        this.dohvatiUpite()
      }
  
      dohvatiUpite = () => {
        fetch('http://localhost:8080/upiti', {
            method: 'GET',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
            }})
        .then((response) => response.json()).then(response => {
          console.log(response);
          
          this.setState({
                upiti: response
          })
      });
      }

      otvoriUpit = (id) => {
            this.props.history.push("/upiti/odgovori/" + id);
      }
      
      render() {
        const role = sessionStorage.getItem('uloga');
        const admin = role === "ROLE_ADMIN" || role === "ROLE_DJELATNIK"
        if (!admin) {
            return (
                <div>
                    <h3> NOT FOUND </h3>
                </div>
            )
        }
        const upiti = this.state.upiti.map(u => <h3 onClick={() => this.otvoriUpit(u.id)}> {u.naslov}  </h3>)
        return (
          <div > 
            {upiti}
          </div> 
        )
          
      }
  
  
}

export default withRouter(Upiti)