import React, {Component} from "react"
import { FilePond, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css';
import { withRouter } from "react-router-dom";
import Select from 'react-select'
registerPlugin(FilePondPluginImagePreview);

class IzmjenaPredmeta extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            files: [],
            slike: [],
            naziv:"",
            cijena:"",
            uPonudi: true,
            posebnaPonuda: false,
            disabled: true,
            klijent: "",
            klijenti: [],
            predmet: null,
            id: props.match.params.predmetID,
            available: true,
            datumUvrstenja: ""
        }
    }

    componentDidMount() {
        const role = sessionStorage.getItem('uloga');
        const username = sessionStorage.getItem('korisnik');

        fetch("http://localhost:8080/predmeti/predmet/?id="+this.state.id + "&role=" + role + "&username=" + "" )
        .then((response) => {
            if (response.status === 400 || response.status === 500) {
                this.setState({
                    available: false
                })
            }
            return response.json()}).then(response => {
          console.log("rr:" + response.cijena);

          this.setState({
              predmet: response,
            naziv: response.imePredmeta,
            cijena: response.cijena,
            uPonudi: response.uPonudi,
            slike: response.slike,
            posebnaPonuda: response.posebnaPonuda,
            klijent: this.state.available ? response.klijent.korisnickoIme : "",
            datumUvrstenja: response.datumUvrstenja

          })
          console.log("date1 " + this.state.datumUvrstenja)



      });
        fetch('http://localhost:8080/klijenti', {
            method: 'GET',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
            }})
        .then((response) => response.json()).then(response => {

          this.setState({
              klijenti: response
          })
      });
    }

    handleChange = (event) => {
        const {name, value, type, checked} = event.target
        type === "checkbox" ?
            this.setState({
                [name]: checked
            })
        :
        this.setState({
            [name]: value
        })
    }

    dodajPredmet = () => {

        this.state.files.map(file => {
            this.setState({slike: []})
            var formData = new FormData();
            formData.append("file", file);

            fetch("http://localhost:8080/files/post", {
                method: 'POST',
                body:formData
            }).then(response => {
            })

            fetch("http://localhost:8080/slike/registriraj", {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
                },
                body: JSON.stringify({
                  "put": file.name
                })
                }).then((response) => response.json()).then(response => {


                this.setState(ps => {
                    let slike = ps.slike
                    const id = {
                        id: response.id
                    }
                    slike.push(id)
                    return {
                        slike: slike
                    }
                })
              }
              )
        })
        setTimeout(() => {
            fetch("http://localhost:8080/predmeti/azuriraj", {
                    method: 'POST',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json',
                      'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
                    },
                    body: JSON.stringify({
                        "id": this.state.id,
                        "datumUvrstenja": this.state.datumUvrstenja,
                      "imePredmeta": this.state.naziv,
                      "cijena":this.state.cijena,
                      "posebnaPonuda":this.state.posebnaPonuda,
                      "uPonudi":this.state.uPonudi,
                      "slike": this.state.slike,
                      "klijent": this.state.klijenti.find(k => {
                          return k.korisnickoIme === this.state.klijent
                      })
                    })
                  }).then(response => {

                  })
                  console.log("date " + this.state.datumUvrstenja)
                  window.location.reload()
          }, 1000);

    }
    render() {
        const role = sessionStorage.getItem('uloga');
        const admin = role === "ROLE_ADMIN" || role === "ROLE_DJELATNIK"
        if (this.state.predmet === null || !this.state.available || !admin) {
            return (
                <div>
                    <h3> NOT FOUND </h3>
                </div>
            )
        }
        const slike = this.state.predmet.slike.map(s => <img src={"http://localhost:8080/files/files/" + s.put} alt="greska"/>)
        const klijenti = this.state.klijenti.map(k => {
            return {
                label: k.korisnickoIme,
                value: k.korisnickoIme
            }
        })
        const defaultKlijent = {
            label: this.state.predmet.klijent.korisnickoIme,
            value: this.state.predmet.klijent.korisnickoIme
        }
        return (
            <div>
               <FilePond ref="pond" allowMultiple={true} data-max-files="5"
                            onupdatefiles={(fileItems) => {
                                let num = 0;
                                fileItems.forEach(f => num++
                                )
                              this.setState({
                                  files: fileItems.map(fileItem => fileItem.file),
                                  disabled: num > 0 ? false : true
                              });


                          }}/>
                <form>
                    <input
                        name="naziv"
                        value={this.state.naziv}
                        onChange={this.handleChange}
                        placeholder="Naziv predmeta"
                    />
                    <br />



                    <input
                        name="cijena"
                        value={this.state.cijena}
                        onChange={this.handleChange}
                        placeholder="Cijena"
                    />
                    <br />




                    <label>
                        <input
                            type="checkbox"
                            name="uPonudi"
                            onChange={this.handleChange}
                            checked={this.state.uPonudi || this.state.posebnaPonuda}
                        /> U ponudi?
                    </label>
                    <br />

                    <label>
                        <input
                            type="checkbox"
                            name="posebnaPonuda"
                            onChange={this.handleChange}
                            checked={this.state.posebnaPonuda}
                        /> Posebna ponuda?
                    </label>
                    <br />

                    <Select defaultValue={defaultKlijent} onChange={(s) => this.setState({klijent: s.value})} placeholder="Klijent"  options={klijenti } />

                    <br />


                </form>
                {slike}
                <br/>
                <button  disabled={this.state.disabled}  onClick={this.dodajPredmet}> Dodaj </button>
            </div>
        );
    }
}

export default withRouter(IzmjenaPredmeta)
