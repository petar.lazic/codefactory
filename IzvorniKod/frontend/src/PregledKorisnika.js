import React, {Component} from "react"
import { withRouter } from "react-router-dom";
import Select from 'react-select'

import { NavLink } from 'react-router-dom'



class PregledKorisnika extends React.Component {
    constructor() {
        super()
        this.state = {
            tip:"svi",
            klijenti:[],
            djelatnici:[]
        }
    }

    componentDidMount(){
        const role = sessionStorage.getItem('uloga');
        const admin = role === "ROLE_ADMIN"

        if(admin) {
            fetch('http://localhost:8080/klijenti', {
            method: 'GET',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
            }})
        .then((response) => response.json()).then(response => {
          console.log(response);
          
          this.setState({
              klijenti: response
          })
        });
      fetch('http://localhost:8080/djelatnici', {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
        }})
        .then((response) => response.json()).then(response => {
        console.log(response);
        
        this.setState({
            djelatnici: response
            })
        });
        }

        
    }

    compareKorisnici = (a, b) =>{
        var nameA = a.korisnickoIme.toUpperCase(); // ignore upper and lowercase
        var nameB = b.korisnickoIme.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }
        return 0;
      }

    handleChange = (event) => {
        const {name, value} = event.target
        this.setState({
            [name]: value
        })
    }

    handleClick = (tip, username) => {
        console.log("tip " + tip + " " + username)
        this.props.history.push('/izmjenaProfila/' + tip + "/" + username)
    }


    render() {
        const role = sessionStorage.getItem('uloga');
        const admin = role === "ROLE_ADMIN"
        if (!admin) {
            return (
                <div> 
                    <h3> NOT FOUND </h3>
                </div>
            )
        }
        
        let lista = []
        if (this.state.tip === "svi") {
            lista = this.state.klijenti.concat(this.state.djelatnici).sort(this.compareKorisnici).map(kd => {
                const tip = kd.administrator != null ? "djelatnik":"klijent"
                const username = kd.korisnickoIme
                const ime = kd.ime
                const prezime = kd.prezime
                const email = kd.ePosta
                const kontakt = kd.kontaktBroj
                const link = '/izmjenaProfila/' + tip + "/" + username
                return <tr >
                <td style={{border: "1px solid black"}}> {ime} </td> 
                <td style={{border: "1px solid black"}}> {ime} </td>
                <td style={{border: "1px solid black"}}> {username} </td>
                <td style={{border: "1px solid black"}}> {email} </td>
                <td style={{border: "1px solid black"}}> {kontakt} </td>
                <td style={{border: "1px solid black"}}> {tip} </td>
                <td style={{border: "1px solid black"}}> <NavLink exact to = {link} > izmjeni </NavLink> </td>
                
            </tr>
            })
        }
        else if (this.state.tip === "djelatnik") {
            lista = this.state.djelatnici.sort(this.compareKorisnici).map(kd => {
                const tip = "djelatnik"
                const username = kd.korisnickoIme
                const ime = kd.ime
                const prezime = kd.prezime
                const email = kd.ePosta
                const kontakt = kd.kontaktBroj
                const link = '/izmjenaProfila/' + tip + "/" + username
                return <tr >
                <td style={{border: "1px solid black"}}> {ime} </td> 
                <td style={{border: "1px solid black"}}> {ime} </td>
                <td style={{border: "1px solid black"}}> {username} </td>
                <td style={{border: "1px solid black"}}> {email} </td>
                <td style={{border: "1px solid black"}}> {kontakt} </td>
                <td style={{border: "1px solid black"}}> {tip} </td>
                <td style={{border: "1px solid black"}}> <NavLink exact to = {link} > izmjeni </NavLink> </td>
                
            </tr>
            })
        }
        else {
            lista = this.state.klijenti.sort(this.compareKorisnici).map(kd => {
                const tip = "klijent"
                const username = kd.korisnickoIme
                const ime = kd.ime
                const prezime = kd.prezime
                const email = kd.ePosta
                const kontakt = kd.kontaktBroj
                const link = '/izmjenaProfila/' + tip + "/" + username
                return <tr >
                <td style={{border: "1px solid black"}}> {ime} </td> 
                <td style={{border: "1px solid black"}}> {ime} </td>
                <td style={{border: "1px solid black"}}> {username} </td>
                <td style={{border: "1px solid black"}}> {email} </td>
                <td style={{border: "1px solid black"}}> {kontakt} </td>
                <td style={{border: "1px solid black"}}> {tip} </td>
                <td style={{border: "1px solid black"}}> <NavLink exact to = {link} > izmjeni </NavLink> </td>
                
            </tr>
            })
        }
        const tipoviKorisnika = [{label:"Klijenti", value:"klijent"}, {label:"Djelatnici", value: "djelatnik"}, {label:"Svi", value: "svi"}]
        const defaultTip = {
            label: "Svi",
            value: "svi"
        }
        return (
            <div >
                
                <Select onChange={(s) => this.setState({tip: s.value})} defaultValue={defaultTip} placeholder="Tip korisnika"  options={tipoviKorisnika } />
                  
                <br/>
                
                <table style={{width:"75%", marginLeft: "auto", marginRight: "auto", padding: "15px", textAlign: "center", borderSpacing: "0px", fontSize:"25px"}}>
                
                <tr >
                  <th style={{border: "1px solid black"}}>IME</th>
                  <th style={{border: "1px solid black"}}>PREZIME</th>
                  <th style={{border: "1px solid black"}}>KORISNIČKO IME</th> 
                  <th style={{border: "1px solid black"}}>E-POŠTA</th>
                  <th style={{border: "1px solid black"}}>KONTAKT BROJ</th>
                  <th style={{border: "1px solid black"}}>TIP</th>
                  <th></th>
                  
                </tr>
                {lista}
              </table>
            </div>
        );
    }
} 

export default withRouter(PregledKorisnika)