import React from 'react'
import Select from 'react-select'
import { withRouter } from "react-router-dom";
import './style.css'

class UnosTransakcije extends React.Component{
    constructor(props){
      super(props);
      this.state = {
          idPredmeta: props.match.params.predmetID,
          korisnickoIme: "",
          iznos: "",
          vrstaTransakcije: "",
          klijenti: [],
          djelatnici:[],
          predmeti: [],
          dugovi: []
        }

        }

    componentDidMount() {
      const role = sessionStorage.getItem("uloga");
      if (role == "ROLE_DJELATNIK" || role == "ROLE_ADMIN" ) {
        console.log("MOUNT");
        fetch('http://localhost:8080/klijenti', {
                    method: 'GET',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json',
                      'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
                    }})
                .then((response) => response.json()).then(response => {

                  this.setState({
                      klijenti: response
                  })
              });
              fetch('http://localhost:8080/djelatnici', {
                          method: 'GET',
                          headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                            'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
                          }})
                      .then((response) => response.json()).then(response => {

                        this.setState({
                            djelatnici: response
                        })
                    });
                    fetch('http://localhost:8080/predmeti/', {
                                method: 'GET',
                                headers: {
                                  'Accept': 'application/json',
                                  'Content-Type': 'application/json',
                                  'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
                                }})
                            .then((response) => response.json()).then(response => {

                              this.setState({
                                  predmeti: response
                              })
                          });
                          fetch('http://localhost:8080/dugovi', {
                                      method: 'GET',
                                      headers: {
                                        'Accept': 'application/json',
                                        'Content-Type': 'application/json',
                                        'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
                                      }})
                                  .then((response) => response.json()).then(response => {

                                    this.setState({
                                        dugovi: response
                                    })
                                });
      }
      

            }



 handleChange = (event) => {
    this.setState({
      [event.target.name] : event.target.value
    });
  };

  unesiTransakciju = (event) => {
        console.log(this.state.klijenti)
        console.log(this.state.predmeti)
        console.log(this.state.idPredmeta)
        console.log(sessionStorage.getItem("uloga"))

        fetch("http://localhost:8080/transakcije/kreiraj", {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
            },
            body: JSON.stringify({
              "vrstaTransakcije": this.state.vrstaTransakcije,
              "iznos": this.state.iznos,
              "predmet": this.state.predmeti.find(p => {
                if(p.id == this.state.idPredmeta) {
                  return p;}
                      }),
              "klijent": this.state.klijenti.find(k => {
                          return k.korisnickoIme == this.state.korisnickoIme
                      }),
              "djelatnik":this.state.djelatnici.find(d => {
                          return d.korisnickoIme === sessionStorage.getItem('korisnik')
                      })
            })
		}).then((response) => { return response.json() }). then((responseJson) => {
			
    if(this.state.vrstaTransakcije === "Zalog") this.evidentirajZaduzenje();
    if(this.state.vrstaTransakcije === "Otplata") this.evidentirajOtplatu();
	    if(this.state.vrstaTransakcije === "Kupnja") this.prodajPredmet();

	
					this.props.history.push('/');
					var win = window.open("http://localhost:8080/djelatnici/ugovor/" + responseJson.id, "blank_");
					win.focus();
					
				});
		
		
  }

  evidentirajZaduzenje = (event) => {
    console.log("evidentiram dug")
    console.log(sessionStorage.getItem('btoa'))
    fetch("http://localhost:8080/dugovi/registriraj", {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + sessionStorage.getItem('btoa'),
        },
        body: JSON.stringify({
          "iznos": this.state.iznos,
          "predmet": this.state.predmeti.find(p => {
                      return p.id == this.state.idPredmeta
                  })
        }
      )
      }
    ).then((response) => response.json()).then(response => {
      console.log(response);
    })
  };
  
  prodajPredmet = (event) => {
    var predmet = this.state.predmeti.find(p => {
      if(p.id == this.state.idPredmeta) {
        return p;}
            })
    predmet.uPonudi = false;
    predmet.posebnaPonuda = false;
    fetch("http://localhost:8080/predmeti/azuriraj", {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
        },
        body: JSON.stringify(predmet)
      }
    )
  }

  evidentirajOtplatu = (event) => {
      var dug = this.state.dugovi.find(d => {
                  return d.predmet.id == this.state.idPredmeta
              });
      dug.iznos = dug.iznos - this.state.iznos;
      if (dug.iznos > 0) {
        fetch("http://localhost:8080/dugovi/azuriraj", {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
            },
            body: JSON.stringify(dug)
          }
        )
      } else {
        console.log("brisem")
        console.log(dug.id)
        fetch("http://localhost:8080/dugovi/izbrisi", {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
            },
            body: dug.id
          }
        )
      }
      
  };



  render(){
    const role = sessionStorage.getItem("uloga");
    const admin = role === "ROLE_ADMIN" || role === "ROLE_DJELATNIK"
    console.log("role and admin" + role + " " + admin)
    if (!admin) {
      return (
        <div>
                    <h3> NOT FOUND </h3>
                </div>
            )
          }
          const klijenti = this.state.klijenti.map(k => {
            return {
                label: k.korisnickoIme,
                value: k.korisnickoIme
            }
        })
      const vrsteTransakcije = [{label:"Prodaja", value:"Prodaja"}, {label:"Kupnja", value: "Kupnja"}, {label:"Zalog", value: "Zalog"}, {label: "Otplata", value: "Otplata"}]
      return(
          <div className="opis">
              <h3>Podaci o transakciji</h3>
              <div className="opisPredmeta">
                  <Select onChange={(s) => this.setState({korisnickoIme: s.value})} placeholder="Klijent"  options={klijenti } />
                  <p><strong>Iznos transakcije</strong><input type="number"  name="iznos" class="form-control" className="editable" value={this.state.iznos} onChange={this.handleChange}/></p><hr />
                  <Select onChange={(s) => this.setState({vrstaTransakcije: s.value})} placeholder="Vrsta transakcije" options={vrsteTransakcije} />

                  <button className="dodajButton" onClick={this.unesiTransakciju}>Unesi</button>
              </div>

          </div>
      )
  }
  }

  export default withRouter(UnosTransakcije);
