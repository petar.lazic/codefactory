import React, { Component } from 'react';
import { BrowserRouter, Switch, Route, Link, NavLink} from 'react-router-dom';
import './App.css';
import './header.css'

import HeaderLoggedOut from './HeaderLoggedOut';
import Footer from './Footer';

import DisplayComponent from "./DisplayComponent";
import HeaderLoggedIn from './HeaderLoggedIn';


import { withRouter } from "react-router-dom";


class App extends React.Component {
  constructor(){
    super();
    this.state = {
      loading: true,
      isLoggedIn: false
    }
    
    this.onLogin = this.onLogin.bind(this);
    this.onLogout = this.onLogout.bind(this);
  this.loadCurrentUser = this.loadCurrentUser.bind(this);
   //this.componentDidMount = this.componentDidMount.bind(this);
   
  }
    componentDidMount(){
	  this.setState( {loading: true} );
      this.loadCurrentUser();
    }
	
	loadCurrentUser() {  
		if(!sessionStorage.getItem('korisnik')) {
			this.setState( {loading: false} );
			this.setState( {isLoggedIn: false});
		} else {
			this.setState( {loading: false} );
			this.setState( {isLoggedIn: true});
		}
	}

    onLogin(ime, uloga, lozinka) {
      this.loadCurrentUser();
      sessionStorage.setItem('korisnik', ime);
      sessionStorage.setItem('uloga', uloga);
      sessionStorage.setItem('btoa', btoa(ime + ":" + lozinka));
      console.log("btoa:" + sessionStorage.getItem('btoa'));
      
      this.setState({isLoggedIn: true});
      this.props.history.push('/')
    };

    onLogout() {
      sessionStorage.clear();
	  console.log("stisnut sam");
    this.setState({isLoggedIn: false});
    this.props.history.push('/')
    };

    
    
  
  render() {
    if (this.state.loading){
      return <div><p>Loading...</p></div>
    }

    if(this.state.isLoggedIn){
      return (
        
        <div>
          <HeaderLoggedIn onLogout={this.onLogout} name={sessionStorage.getItem('korisnik')} role={sessionStorage.getItem('uloga')} />
          <DisplayComponent />
          <Footer className="Footer"/>
        </div>
          
      );
    }
    return(
      <div >
          <HeaderLoggedOut onLogin={this.onLogin} className="a"/>
          <DisplayComponent />  
		  <Footer className="Footer"/>
      </div>
    )
  }
}


export default withRouter(App);
