import React from 'react'

import './pregledProfila.css'
import PredmetContainer from "./PredmetContainer"

class PregledProfila extends React.Component{
    constructor(props){
        super(props);
        this.state = {
			loading: true,
            ime: "",
            prezime: "",
            korisnickoIme: "",
            email: "",
            iban: "",
            upit: "",
            kontaktBroj: "",
			admin: "",
            disabled: "",
            editable: "",
            klijent:{},
            predmeti: [],
            transakcije: [],
			error: ""
        }

        this.handleClick = this.handleClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }



    dohvatiTransakcije = () => {
        fetch("http://localhost:8080/transakcije/dohvatiklijentove", {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
                },
                body: JSON.stringify(this.state.klijent)
              }).then((response) => response.json()).then(response => {
                  console.log("3" + response)
               console.log(response)
                if (response != null) this.setState({
                    transakcije: response,
                    loading: false
                })
                this.setState({
                    loading: false
                })
            });
    }
    componentDidMount(){

        



		const body = sessionStorage.getItem('korisnik');
        const role = sessionStorage.getItem('uloga');
        console.log("body:" + body);
        
        const options = {
            method : 'POST',
            headers : {
                'Content-Type' : 'text/plain',
                'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
            },
            body : body
        };
		const path = role === 'ROLE_KLIJENT'? 'klijenti' : 'djelatnici';
		console.log("patho:" + path);
        fetch('http://localhost:8080/' + path +'/profil', options)
            .then((response) => {
                console.log("response: " + response.status);
                
                return response.json()})
			.then((responseJson) => {
                this.setState({klijent: responseJson})
                
                this.setState({ime: responseJson.ime});
				this.setState({prezime: responseJson.prezime});
				this.setState({korisnickoIme: responseJson.korisnickoIme});
				this.setState({email: responseJson.ePosta});
				this.setState({iban: responseJson.iban});
				this.setState({kontaktBroj: responseJson.kontaktBroj});
				this.setState({upit: responseJson.brojUpita});
                this.setState({admin: responseJson.administrator});
                console.log("klijent dohvacen")
                console.log("1" + JSON.stringify(this.state.klijent))
                this.dohvatiTransakcije()
            });

    }

 handleChange(event){
    this.setState({
      [event.target.name] : event.target.value
    });
  };
   
  handleClick(event){
        document.getElementsByClassName("podatciProfila")[0].setAttribute("style", "pointer-events: auto")
        const inputs = document.getElementsByClassName("editable")
        for (var x = 0; x < inputs.length; x++) {
            inputs[x].setAttribute("style", "border: 1px solid gray");
        }
        this.setState({ disabled: "false" });
        const submitButton = document.getElementsByClassName("submitButton");
        submitButton[0].style.color = "black";
    }
	
	onSubmit = (e) => {
	  e.preventDefault();
	  const data = {
		ime: this.state.ime,
		prezime: this.state.prezime,
		kontaktBroj: this.state.kontaktBroj,
		ePosta: this.state.email,
		korisnickoIme: this.state.korisnickoIme,
		iban: this.state.iban,
		lozinka: this.state.lozinka,
		matchingLozinka: this.state.lozinka2
	  };
	  const options = {
		method: 'POST',
		headers: {
            'Content-Type' : 'application/json',
            'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
           
		},
		body: JSON.stringify(data)
	  };
	  fetch('http://localhost:8080/klijenti/azuriraj', options)
				.then((response) => {
                if (response.status === 401) {
					//var mess = response.json().message;
					//console.log(mess);
                    this.setState( { error : "Neispravni podatci" });
                } 
                return response.json();
            })
			.then((responseJson) => {
					if(responseJson.korisnickoIme !== sessionStorage.getItem('korisnik')) {
						sessionStorage.clear();
					} 
					window.location.reload();
				});

	} 

    render(){
	
		if (this.state.loading){
      return (<div><p>Loading...</p></div>)
    } else if(!sessionStorage.getItem('korisnik')) {
		return "Nedozvoljen pristup!";
	}else if(this.state.upit >= 0){

        //const predmeti = this.state.predmeti.map(p => <PredmetContainer id={p.id} naziv={p.imePredmeta} datum={p.datumUvrstenja} cijena={p.cijena} uPonudi={p.uPonudi} posebnaPonuda={p.posebnaPonuda}/>)
        let prodaniPredmeti = []
        let založeniPredmeti = []
        let kupljeniPredmeti = []
        let otkupljeniPredmeti = []
        console.log("ispis transakcija " + this.state.transakcije.length )
        this.state.transakcije.forEach(t => {
            switch(t.vrstaTransakcije) {
                case "Prodaja":
                    prodaniPredmeti.push(<PredmetContainer image={"http://localhost:8080/files/files/" + t.predmet.slike[0].put} id={t.predmet.id} naziv={t.predmet.imePredmeta} datum={t.predmet.datumUvrstenja} cijena={t.iznos} uPonudi={t.predmet.uPonudi} posebnaPonuda={t.predmet.posebnaPonuda}  />);
                    break;
                case "Kupnja":
                    kupljeniPredmeti.push(<PredmetContainer image={"http://localhost:8080/files/files/" + t.predmet.slike[0].put} id={t.predmet.id} naziv={t.predmet.imePredmeta} datum={t.predmet.datumUvrstenja} cijena={t.iznos} uPonudi={t.predmet.uPonudi} posebnaPonuda={t.predmet.posebnaPonuda}  />);
                    break;
                case "Otplata":
                    otkupljeniPredmeti.push(<PredmetContainer image={"http://localhost:8080/files/files/" + t.predmet.slike[0].put} id={t.predmet.id} naziv={t.predmet.imePredmeta} datum={t.predmet.datumUvrstenja} cijena={t.iznos} uPonudi={t.predmet.uPonudi} posebnaPonuda={t.predmet.posebnaPonuda}  />);
                    break;
                case "Zalog":
                    založeniPredmeti.push(<PredmetContainer image={"http://localhost:8080/files/files/" + t.predmet.slike[0].put} id={t.predmet.id} naziv={t.predmet.imePredmeta} datum={t.predmet.datumUvrstenja} cijena={t.iznos} uPonudi={t.predmet.uPonudi} posebnaPonuda={t.predmet.posebnaPonuda}  />);
                    break;

            }
        });

        return(
            <div className="profil">
                <h3>Profil</h3>
                <button className = "izmjenaProfilaButton" onClick={this.handleClick}>Izmijeni profil</button>
				
                <div className="podatciProfila">
                    <p><strong>Ime</strong><input type="text" name="ime" className="editable" value={this.state.ime} onChange={this.handleChange} pattern="[a-zA-Z0-9]{3}[a-z0-9]*"/></p><hr />
                    <p><strong>Prezime</strong><input type="text" name="prezime" className="editable" value={this.state.prezime} onChange={this.handleChange} pattern="[a-zA-Z0-9]{3}[a-z0-9]*"/></p><hr />
                    <p><strong>Korisničko ime</strong><input type="text" name="korisnickoIme" className="editable" value={this.state.korisnickoIme} onChange={this.handleChange} pattern="[a-zA-Z0-9]{6}[a-z0-9]*"/></p><hr />
                    <p><strong>Adresa e-pošte</strong><input type="text" name="email" className="editable" value={this.state.email} onChange={this.handleChange} pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"/></p><hr />
                    <p><strong>Broj računa-IBAN</strong><input type="text" name="iban" className="editable" value={this.state.iban} onChange={this.handleChange} pattern="^\s*$|^HR[0-9]{19}"/></p><hr />
                    <p><strong>Broj telefona</strong><input type="text" name="kontaktBroj" className="editable" value={this.state.kontaktBroj} onChange={this.handleChange}/></p><hr />


                    <p><input type="submit" value="Spremi promjene" className="submitButton" onClick={this.onSubmit} color="transparent"/></p>

                    <div>
                        <p style={{color: 'red'}}> {this.state.error} </p>  
                    </div>
                    <br/>
                    <br/>

                    <p>  </p>
					<p>  </p>
				</div>
                <div>
                        <table style={{width:"75%", marginLeft: "auto", marginRight: "auto", padding: "5px", textAlign: "center", borderSpacing: "0px", fontSize:"25px"}}>
                        <caption> <p><strong>Kupljeni Predmeti</strong></p></caption>
                            <tr >
                            <th style={{border: "1px solid black"}}>SLIKA</th>
                            <th style={{border: "1px solid black"}}>ID</th>
                            <th style={{border: "1px solid black"}}>PREDMET</th> 
                            <th style={{border: "1px solid black"}}>IZNOS (HRK)</th>
                            <th style={{border: "1px solid black"}}>DATUM DODAVANJA</th>
                            </tr>
                            {kupljeniPredmeti}
                        </table>
                        <table style={{width:"75%", marginLeft: "auto", marginRight: "auto", padding: "5px", textAlign: "center", borderSpacing: "0px", fontSize:"25px"}}>
                        <caption> <p><strong>Prodani Predmeti</strong></p></caption>
                            <tr >
                            <th style={{border: "1px solid black"}}>SLIKA</th>
                            <th style={{border: "1px solid black"}}>ID</th>
                            <th style={{border: "1px solid black"}}>PREDMET</th> 
                            <th style={{border: "1px solid black"}}>IZNOS (HRK)</th>
                            <th style={{border: "1px solid black"}}>DATUM DODAVANJA</th>
                            </tr>
                            {prodaniPredmeti}
                        </table>
                        <table style={{width:"75%", marginLeft: "auto", marginRight: "auto", padding: "5px", textAlign: "center", borderSpacing: "0px", fontSize:"25px"}}>
                        <caption> <p><strong>Založeni Predmeti</strong></p></caption>
                            <tr >
                            <th style={{border: "1px solid black"}}>SLIKA</th>
                            <th style={{border: "1px solid black"}}>ID</th>
                            <th style={{border: "1px solid black"}}>PREDMET</th> 
                            <th style={{border: "1px solid black"}}>IZNOS (HRK)</th>
                            <th style={{border: "1px solid black"}}>DATUM DODAVANJA</th>
                            </tr>
                            {založeniPredmeti}
                        </table>
                        <table style={{width:"75%", marginLeft: "auto", marginRight: "auto", padding: "5px", textAlign: "center", borderSpacing: "0px", fontSize:"25px"}}>
                        <caption> <p><strong>Otkupljeni Predmeti</strong></p></caption>
                            <tr >
                            <th style={{border: "1px solid black"}}>SLIKA</th>
                            <th style={{border: "1px solid black"}}>ID</th>
                            <th style={{border: "1px solid black"}}>PREDMET</th> 
                            <th style={{border: "1px solid black"}}>IZNOS (HRK)</th>
                            <th style={{border: "1px solid black"}}>DATUM DODAVANJA</th>
                            </tr>
                            {otkupljeniPredmeti}
                        </table>
                        
                    </div>
            </div>
        )
    } else {
		let a = '';
		if (this.state.admin) a = <p><strong>* Djelatnik je administrator sustava.</strong></p>
		return(
            <div className="profil">
                <h3>Profil</h3>
                
                <div className="podatciProfila">
                    <p><strong>Ime</strong><input type="text" name="ime" className="editable" value={this.state.ime} onChange={this.handleChange}/></p><hr />
                    <p><strong>Prezime</strong><input type="text" name="prezime" className="editable" value={this.state.prezime} onChange={this.handleChange}/></p><hr />
                    <p><strong>Korisničko ime</strong><input type="text" name="korisnickoIme" className="editable" value={this.state.korisnickoIme} onChange={this.handleChange}/></p><hr />
                    <p><strong>Adresa e-pošte</strong><input type="text" name="email" className="editable" value={this.state.email} onChange={this.handleChange}/></p><hr />
                    <p><strong>Broj telefona</strong><input type="text" name="kontaktBroj" className="editable" value={this.state.kontaktBroj} onChange={this.handleChange}/></p><hr />
                    

                    <p>  </p>
					 {a}
					<p>  </p>
					<p>  </p>
                </div>
            </div>
        )
	}
	}
}

export default PregledProfila;

