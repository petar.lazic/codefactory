import React from "react"

class Predmet extends React.Component {

    constructor(props) {
        super()
        this.state = {
            predmet: null,
            id: props.match.params.predmetID,
            available: true
        }
    }

    componentDidMount() {
        console.log("mounted");
        const role = sessionStorage.getItem('uloga');
        const username = sessionStorage.getItem('korisnik');

        fetch("http://localhost:8080/predmeti/predmet/?id="+this.state.id + "&role=" + role + "&username=" + username )
        .then((response) => {
            if (response.status === 400 || response.status === 500) {
                console.log("status " + response)
                this.setState({
                    available: false
                })
            }
            return response.json()}).then(response => {
          console.log("rr:" + response);

          this.setState({
              predmet: response
          })



      });
    }


    napraviTransakciju = (event) => {
      this.props.history.push("/unosTransakcije/" + this.state.id);

    }

    render() {
        if (this.state.predmet === null || !this.state.available) {
            return (
                <div>
                    <h3> NOT FOUND </h3>
                </div>
            )
        }

            const slike = this.state.predmet.slike.map(s => <img src={"http://localhost:8080/files/files/" + s.put} alt="greska"/>)
            const role = sessionStorage.getItem('uloga');
            return (
                <div style={{display: "flex", flexDirection: "column", alignItems: "center", margin: "auto"}}>

                    <h2 style={{height: "2px"}}>{this.state.predmet.imePredmeta} </h2>
                    <h4 style={{height: "2px"}}>id: {this.state.predmet.id} </h4>
                    {role === "ROLE_ADMIN" || role === "ROLE_DJELATNIK" ?  <h4 style={{height: "2px"}}>klijent: {this.state.predmet.klijent.korisnickoIme} </h4>: ""}
                    <h4 style={{height: "2px"}}>datum uvrstenja: {this.state.predmet.datumUvrstenja} </h4>
                    <h3 style={{height: "2px"}}>cijena: {this.state.predmet.cijena} HRK</h3>
                    {role === "ROLE_ADMIN" || role === "ROLE_DJELATNIK" ? <button className="napraviTransakciju" onClick={this.napraviTransakciju}>Napravi transakciju</button>: ""}
                    {slike}
                </div>
            )


    }
}

export default Predmet
