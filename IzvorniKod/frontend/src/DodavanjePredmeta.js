import React, {Component} from "react"
import { FilePond, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css';
import { withRouter } from "react-router-dom";
import Select from 'react-select'
registerPlugin(FilePondPluginImagePreview);

class DodavanjePredmeta extends React.Component {
    constructor() {
        super()
        this.state = {
            files: [],
            slike: [],
            naziv:"",
            cijena:"",
            uPonudi: true,
            posebnaPonuda: false,
            disabled: true,
            klijent: "",
            klijenti: [],
            djelatnik: {},
            fileNumber: 0,
            filePondEnabled: true
        }
    }

    componentDidMount() {

        fetch('http://localhost:8080/klijenti', {
            method: 'GET',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
            }})
        .then((response) => response.json()).then(response => {
          console.log(response);

          this.setState({
              klijenti: response
          })
        });
        const username = sessionStorage.getItem('korisnik');
        const url = 'http://localhost:8080/djelatnici/profil'
        console.log("url: " + url);
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type' : 'text/plain',
                'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
            },
            body: username})
        .then((response) => response.json()).then(response => {
          console.log("response name: " + response.korisnickoIme);

          this.setState({
              djelatnik: response
          })
      });
    }

    handleChange = (event) => {
        const {name, value, type, checked} = event.target
        type === "checkbox" ?
            this.setState({
                [name]: checked
            })
        :
        this.setState({
            [name]: value
        })
    }

    dodajPredmet = () => {
        var i = 0;
        this.state.files.map(file => {
            var formData = new FormData();
            var d = new Date();
            var n = d.getTime();

            const file2 = new File([file], n + file.name.substring(file.name.lastIndexOf(".")), {type: file.type});
            console.log("file " + file2.name + " " + file2.type)

            formData.append("file", file2);

            fetch("http://localhost:8080/files/post", {
                method: 'POST',
                body:formData
            }).then(response => {
                console.log("file uploaded");

            })
            fetch("http://localhost:8080/slike/registriraj", {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
                },
                body: JSON.stringify({
                  "put": file2.name
                })
                }).then((response) => response.json()).then(response => {
                    console.log("slika added");
                    i++;
                this.setState(ps => {
                    let slike = ps.slike
                    const id = {
                        id: response.id
                    }
                    slike.push(id)
                    return {
                        slike: slike
                    }
                })
                if (i == this.state.fileNumber) {
                    this.kreirajPredmet()
                }
              }
              )




        
    })
    }

    kreirajPredmet = () => {
        fetch("http://localhost:8080/predmeti/kreiraj", {
                    method: 'POST',
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json',
                      'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
                    },
                    body: JSON.stringify({
                      "imePredmeta": this.state.naziv,
                      "cijena":this.state.cijena,
                      "posebnaPonuda":this.state.posebnaPonuda,
                      "uPonudi":this.state.uPonudi,
                      "slike": this.state.slike,
                      "klijent": this.state.klijenti.find(k => {
                          return k.korisnickoIme === this.state.klijent
                      })
                    })
                    }).then((response) => response.json()).then(response => {
                      console.log(response);
                      this.props.history.push("/unosTransakcije/" + response.id);
                    })
    }

    

    render() {
        const role = sessionStorage.getItem('uloga');
        const admin = role === "ROLE_ADMIN" || role === "ROLE_DJELATNIK"
        if (!admin) {
            return (
                <div>
                    <h3> NOT FOUND </h3>
                </div>
            )
        }


        const klijenti = this.state.klijenti.map(k => {
            return {
                label: k.korisnickoIme,
                value: k.korisnickoIme
            }
        })



        return (
            <div>
               <FilePond type="file" allowBrowse={this.state.filePondEnabled} allowPaste={this.state.filePondEnabled} allowDrop={this.state.filePondEnabled} ref="pond" allowMultiple={true} dataMaxFiles={5}
                            onupdatefiles={(fileItems) => {
                                let num = 0;
                                var files = []
                                fileItems.forEach(f => {
                                    console.log(f.file)
                                    if (f.file.type.includes("image") && num < 5) {
                                        files.push(f)
                                        num++}
                                    }
                                )
                                const i = num
                              this.setState({
                                  files: files.map(fileItem => fileItem.file),
                                  fileNumber: num,
                                  filePondEnabled: i < 5 ? true : false,
                                  disabled: num > 0 ? false : true
                              });
                              console.log("number of files " + num)
                              
                              
                              console.log("less " + num > 5)

                          }}/>
                <form>
                    <input
                        name="naziv"
                        value={this.state.naziv}
                        onChange={this.handleChange}
                        placeholder="Naziv predmeta"
                    />
                    <br />



                    <input
                        name="cijena"
                        value={this.state.cijena}
                        onChange={this.handleChange}
                        placeholder="Cijena"
                    />
                    <br />




                    <label>
                        <input
                            type="checkbox"
                            name="uPonudi"
                            onChange={this.handleChange}
                            checked={this.state.uPonudi || this.state.posebnaPonuda}
                        /> U ponudi?
                    </label>
                    <br />

                    <label>
                        <input
                            type="checkbox"
                            name="posebnaPonuda"
                            onChange={this.handleChange}
                            checked={this.state.posebnaPonuda}
                        /> Posebna ponuda?
                    </label>
                    <br />
                    <Select onChange={(s) => this.setState({klijent: s.value})} placeholder="Klijent"  options={klijenti } />
                    <br/>


                </form>
                <button  disabled={this.state.disabled}  onClick={this.dodajPredmet}> Dodaj </button>
            </div>
        );
    }
}

export default withRouter(DodavanjePredmeta)
