import React, {Component} from "react"
import { withRouter } from "react-router-dom";
import Select from 'react-select'

class IzmjenaProfila extends React.Component {
    constructor(props) {
        super()
        this.state = {
            available: false, 
            username: props.match.params.username,
            tip: props.match.params.tip,
            korisnik:{},
            ime : '', 
            prezime : '',
            kontaktBroj: '',
            email : '',
            korisnickoIme : "",
            iban: "",
            lozinka : '',
            lozinka2 : '',
            errorlozinka2: "lozinka prekratka, najmanje 5 znakova",
            erroremail: "",
            errorkorisnickoIme: "",
            errorime: "",
            errorprezime: "",
            klijenti:[],
            djelatnici:[],
            admin: false
        }
    }

    componentDidMount(){
        const role = sessionStorage.getItem('uloga');
        const admin = role === "ROLE_ADMIN"
        if(admin && (this.state.tip == "djelatnik" || this.state.tip == "klijent") ) {
            
            const username = this.state.username
            const tip = this.state.tip
            fetch('http://localhost:8080/klijenti', {
                method: 'GET',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
                }})
            .then((response) => response.json()).then(response => {
              console.log(response);
              
              this.setState({
                  klijenti: response
              })
              var korisnik = {}
              if (tip == "klijent") {
                  korisnik = this.state.klijenti.find(k => k.korisnickoIme == username)
                  if (typeof korisnik != "undefined") {
                          this.setState({
                              korisnik: korisnik,
                              ime : korisnik.ime, 
                              prezime : korisnik.prezime,
                              kontaktBroj: korisnik.kontaktBroj,
                              email : korisnik.ePosta,
                              korisnickoIme : korisnik.korisnickoIme ,
                              iban: korisnik.iban,
                              available: true
                          }) 
                  }
                  else {
                    this.setState({available: false})
                }

              }   
            });
            fetch('http://localhost:8080/djelatnici', {
                method: 'GET',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
                }})
                .then((response) => response.json()).then(response => {
                console.log(response);
                
                this.setState({
                    djelatnici: response
                    })
                var korisnik = {}
                if (tip == "djelatnik") {
                    korisnik = this.state.djelatnici.find(d => d.korisnickoIme == username)
                    if (typeof korisnik != "undefined") {
                            this.setState({
                                korisnik: korisnik,
                                admin: korisnik.administrator,
                                ime : korisnik.ime, 
                                prezime : korisnik.prezime,
                                kontaktBroj: korisnik.kontaktBroj,
                                email : korisnik.ePosta,
                                korisnickoIme : korisnik.korisnickoIme ,
                                iban: korisnik.iban,
                                available: true
                            }) 
            
                    }
                    else {
                        this.setState({available: false})
                    }

                }    
                });
        
        }
        

        
    }

    handleClick = () => {
        const data = {
            id: this.state.korisnik.id,
            ime: this.state.ime,
            prezime: this.state.prezime,
            kontaktBroj: this.state.kontaktBroj,
            ePosta: this.state.email,
            korisnickoIme: this.state.korisnickoIme,
            iban: this.state.iban,
            lozinka: this.state.lozinka,
            matchingLozinka: this.state.lozinka2,
            administrator: this.state.admin
          };
          const options = {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json',
              'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
            },
            body: JSON.stringify(data)
          };
          const url = "http://localhost:8080/" + (this.state.tip === "djelatnik" ? "djelatnici" : "klijenti") + "/azuriraj"
          
          fetch(url, options)
            .then(response => {
                console.log("code " + response.json().statusCode)
                if (response.statusCode > 400) alert("failed")

            })
            if (this.state.username == sessionStorage.getItem("korisnik")) {
                sessionStorage.clear()
                this.props.history.push("/")
            }
            else this.props.history.push("/pregledKorisnika/");	
    }

    validateEmail = (email) => {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    handleChange = (event) => {
        console.log("event")
        const {name, value, type, checked} = event.target
        type === "checkbox" ? 
            this.setState({
                [name]: checked
            })
        :
        this.setState({
            [name]: value
        })
        if (this.state.tip === "klijent" && this.state.admin === true) this.setState({admin:false})
        
        const korisnici = this.state.djelatnici.map(d => d.korisnickoIme).concat(this.state.klijenti.map(k => k.korisnickoIme)).filter(k => k != this.state.korisnik.korisnickoIme)
        const emails = this.state.djelatnici.map(d => d.ePosta).concat(this.state.klijenti.map(k => k.ePosta)).filter(k => k != this.state.korisnik.ePosta)
        console.log("korsinici and emails " + korisnici + "|" + emails);
        console.log("current korisnik " + this.state.korisnik.ime + " " + this.state.korisnik.korisnickoIme)    
        this.state.klijenti.forEach(k => console.log("klijent tijekom provjere " + k.ime))
        switch(name) {
            case "lozinka":
                console.log("len1 " + value.length  +" "+ this.state.lozinka2.length)
                if (value.length < 5  && this.state.lozinka2.length < 5) this.setState({["errorlozinka2"]: "lozinka prekratka, najmanje 5 znakova"})
                else if ((value.length > 4 || this.state.lozinka2.length > 4) && value != this.state.lozinka2) this.setState({["errorlozinka2"]: "lozinke se ne podudaraju"})
                else this.setState({["errorlozinka2"]: ""}) 
                break;
            case "lozinka2": 
                console.log("len2 " +  this.state.lozinka.length +" "+ value.length)
                if (value.length < 5  && this.state.lozinka.length < 5) this.setState({["errorlozinka2"]: "lozinka prekratka, najmanje 5 znakova"})
                else if ((value.length > 4 || this.state.lozinka.length > 4) && value != this.state.lozinka ) this.setState({["errorlozinka2"]: "lozinke se ne podudaraju"})
                else this.setState({["errorlozinka2"]: ""}) 
                break;
            case "email":
                if (!this.validateEmail(value) && !emails.includes(value)) this.setState({["error"+name]: "e-mail nije valjan"})
                else if (emails.includes(value) && this.validateEmail(value)) this.setState({["error"+name]: "email zauzet"})
                else this.setState({["error"+name]: ""})    
                break;
             
            case "korisnickoIme":
                if (value.length < 4 && !korisnici.includes(value)) this.setState({["error"+name]: "korisnicko ime prekratko, najmanje 4 znaka"})
                else if (value.length > 3 && korisnici.includes(value)) this.setState({["error"+name]: "korisnicko ime zauzeto"})
                else this.setState({["error"+name]: ""})    
                break;
            case "ime":
                if (value.length < 3) this.setState({["error"+name]: "ime prekratko, najmanje 3 znaka"})
                else this.setState({["error"+name]: ""})    
                break;
            case "prezime":
                if (value.length < 2) this.setState({["error"+name]: "prezime prekratko, najmanje 2 znaka"})
                else this.setState({["error"+name]: ""})    
                break;
            
        }
    }



    render() {
        const role = sessionStorage.getItem('uloga');
        const admin = role === "ROLE_ADMIN"
        console.log("avail3: " + this.state.available)
        if (!admin || !this.state.available) {
            return (
                <div> 
                    <h3> NOT FOUND </h3>
                </div>
            )
        }
        
        const disabled =    this.state.erroremail.toString().length > 0 || 
                            this.state.errorkorisnickoIme.toString().length > 0 || 
                            this.state.errorlozinka2.toString().length > 0 ||
                            this.state.errorime.toString().length > 0 ||
                            this.state.errorprezime.toString().length > 0
        
        
        return (
            <div>
               
               <form 
                        marginRight="10px" marginLeft="20px">
						<p><strong>Ime</strong></p>
                    <input 
                        name="ime" 
                        value={this.state.ime} 
                        onChange={this.handleChange} 
                        placeholder="Ime" 
						pattern="[a-zA-Z0-9]{3}[a-z0-9]*"
                    />
                    <p style={{color:"crimson", marginLeft:"10px", fontSize: "15px"}}> {this.state.errorime} </p>
          
                    <p><strong>Prezime</strong></p>
                    <input 
                        name="prezime" 
                        value={this.state.prezime}
                        onChange={this.handleChange} 
                        placeholder="Prezime" 
						pattern="[a-zA-Z0-9]{3}[a-z0-9]*"
                    />
                    <p style={{color:"crimson", marginLeft:"10px", fontSize: "15px"}}> {this.state.errorprezime} </p>
                   
					<p><strong>Elektronička pošta</strong> </p>
                    <input
                        name="email" 
                        value={this.state.email}
                        onChange={this.handleChange} 
                        placeholder="e-mail"
						pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                    /> 
                    <br />
					<p style={{color:"crimson", marginLeft:"10px", fontSize: "15px"}}> {this.state.erroremail} </p>
					
                    <p><strong>Korisničko ime</strong></p>
                    <input
                        name="korisnickoIme" 
                        value={this.state.korisnickoIme}
                        onChange={this.handleChange} 
                        placeholder="Korisnicko Ime"
						pattern="[a-zA-Z0-9]{6}[a-z0-9]*"
                    /> 
                    <br />
					<p style={{color:"crimson", marginLeft:"10px", fontSize: "15px"}}> {this.state.errorkorisnickoIme} </p>

					<p><strong>Lozinka</strong></p>
                    <input 
                        name="lozinka" 
                        value={this.state.lozinka}
                        onChange={this.handleChange} 
                        placeholder="lozinka"
                        type = "password"
						pattern="(.){6}(.)*"
                    />
                    <br />
                    <p style={{color:"crimson", marginLeft:"10px", fontSize: "15px"}}> {this.state.errorlozinka2} </p>

					<p><strong>Ponovljena lozinka</strong></p>
                    <input 
                        name="lozinka2" 
                        value={this.state.lozinka2}
                        onChange={this.handleChange} 
                        placeholder="ponovi lozinku"
                        type = "password"
						pattern="(.){6}(.)*"
                    />
                    <br />
                    <p style={{color:"crimson", marginLeft:"10px", fontSize: "15px"}}> {this.state.errorlozinka2} </p>

					<p><strong>Kontakt broj</strong></p>
                    <input
                        name="kontaktBroj" 
                        value={this.state.kontaktBroj}
                        onChange={this.handleChange} 
                        placeholder="Kontakt Broj"
						pattern="[0-9]{3}[0-9]*"
                    /> 
                    <br />
                    
					<p><strong>IBAN</strong></p>
                    <input 
                        name="iban" 
                        value={this.state.iban}
                        onChange={this.handleChange} 
                        placeholder="iban" 
						pattern="^\s*$|^HR[0-9]{19}"						
                    />
                    <br />
					
                    
                    <br />
                    {this.state.tip == "djelatnik" ? <label>
                        <input 
                            type="checkbox"
                            name="admin"
                            onChange={this.handleChange}
                            checked={this.state.admin}
                        /> Admin?
                    </label> : ""}
                    <br/>
                    
                    
                </form>
                
                <button disabled={disabled} onClick={this.handleClick}> Izmjeni </button>
            </div>
        );
    }
} 

export default withRouter(IzmjenaProfila)