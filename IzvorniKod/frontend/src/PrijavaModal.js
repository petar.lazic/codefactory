import ReactModal from "react-modal"
import React from "react"

import './header.css'
import './Modal.css'
import Login from './Login'

class PrijavaModal extends React.Component {
    constructor () {
      super();
      this.state = {
        showModal: false
      };
      
      this.handleOpenModal = this.handleOpenModal.bind(this);
      this.handleCloseModal = this.handleCloseModal.bind(this);
    }
    
    handleOpenModal (event) {
          this.setState({ showModal: true });
    }
    
    handleCloseModal (event) {
      this.setState({ showModal: false });
    }
    
    render () {
      return (
        <div className="divButtonRegPrijava">
          <button onClick={this.handleOpenModal} className="buttonRegPrijava">Prijavi se</button>
          <ReactModal 
             name="showModalLogin"
             isOpen={this.state.showModal}
             contentLabel="showModalLogin"
             className="Modal"
             onRequestClose={this.handleCloseModal}
             shouldCloseOnOverlayClick={true}
          >
            <Login onLogin={this.props.onLogin} className="divLogin"/>
            <button onClick={this.handleCloseModal} >Close</button>
          </ReactModal>
        </div>
      );
    }
  }
  
  const props = {};
  
  export default PrijavaModal