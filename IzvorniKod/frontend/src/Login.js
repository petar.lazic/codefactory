import React from "react"
import ReactDOM from "react-dom"

class Login extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            korisnickoIme : '',
            lozinka : '',
            error : ""
        };

        this.handleChange = this.handleChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
	
	validateForm() {
		return this.state.korisnickoIme.length > 0 && this.state.lozinka.length > 0;
	}

    handleChange(event){
        this.setState(
            {[event.target.name] : event.target.value}
            );
    }
    
    onSubmit(e){
        e.preventDefault();
		console.log("DALJE")
	    const body = '{"username":"' + this.state.korisnickoIme + '","password":"'+this.state.lozinka + '"}';
		console.log(body);
        const options = {
            method : 'POST',
            headers : {
                'Content-Type' : 'application/json'
            },
            body : body
        };
        fetch('http://localhost:8080/auth/login', options)
            .then((response) => {
                if (response.status === 401) {
                    this.setState( { error : "kriva lozinka ili korisničko ime" });
                }
                return response.json()
            })
			.then((responseJson) => {
                this.props.onLogin(responseJson.korisnickoIme, responseJson.uloga, responseJson.lozinka);
                
            });
    }

    render(){
        return(
            <div className = 'Login' >
                <form onSubmit = {this.onSubmit} className="formInput">
                    <div className = 'FormRow'>
                        <label>Korisničko ime</label>
                      <tr><td> <input name = 'korisnickoIme' onChange = {this.handleChange} value = {this.state.korisnickoIme} /></td></tr>
                    </div>
                    <div className = 'FormRow'>
                        <label>Lozinka</label>
                       <tr><td><input name = 'lozinka' type = "password" onChange = {this.handleChange} value = {this.state.lozinka} /></td></tr> 
                    </div>
                    <div>
                        <p style={{color: 'red'}}> {this.state.error} </p>  
                    </div>
                    <hr></hr>
                  <button type = 'submit' id="submitButton">Prijavi se</button>
                </form>
            </div>
        );
    }
}


export default Login;