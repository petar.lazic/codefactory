import React, {Component} from "react"
import { FilePond, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css';
import { withRouter } from "react-router-dom";
import Select from 'react-select'
registerPlugin(FilePondPluginImagePreview);

class Upit extends React.Component {
    constructor(props) {
        super()
        this.state = {
            naslov:"",
            email:"",
            opis:"",
            klijent: false,
            erroremail: "* Adresa E-Pošte nije valjana",
            errornaslov: "* Naslov mora sadržati najmanje 10 znakova",
            erroropis: "* Tijelo upita mora biti dugačko najmanje 20 znakova"
        }
    }

    componentDidMount() {
        const username = sessionStorage.getItem('korisnik');
        if (typeof username != "undefined") {
            console.log("undefined")
            fetch('http://localhost:8080/klijenti/profil', {
                method : 'POST',
                headers : {
                    'Content-Type' : 'text/plain',
                    'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
                },
                body : username
            })
            .then((response) => {
                console.log("response: " + response.status);
                return response.json()})
			.then((responseJson) => {
                console.log("respJSon: " + responseJson.ime);
                this.setState({
                    klijent: true,
                    email: responseJson.ePosta,
                    emailerror:""
                })
            });
        }
    }

    handleChange = (event) => {
        const {name, value, type, checked} = event.target
        type === "checkbox" ?
            this.setState({
                [name]: checked
            })
        :
        this.setState({
            [name]: value
        })

        if (name == "email") {
            if (!this.validateEmail(value)) this.setState({["error"+name]: "* Adresa E-Pošte nije valjana"})
            else this.setState({["error"+name]: ""})
        }
        else if (name == "naslov") {
            if (value.length < 11) this.setState({["error"+name]: "* Naslov mora sadržati najmanje 10 znakova"})
            else this.setState({["error"+name]: ""}) 
        }
        if (name == "opis") {
            if (value.length < 21) this.setState({["error"+name]: "* Tijelo upita mora biti dugačko najmanje 20 znakova"})
            else this.setState({["error"+name]: ""}) 
        }
         
    }

    validateEmail = (email) => {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    posaljiUpit = () => {
        console.log("mail" + this.state.mail)
        fetch("http://localhost:8080/upiti/posaljiupit", {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                  "mail": this.state.email,
                  "naslov": this.state.naslov,
                  "tekstUpita": this.state.opis
                })
                }).then((response) => response.json()).then(response => {
                    console.log("upit poslan");
                    setTimeout(() => this.props.history.push("/"), 500)
              })
    }


    render() {
        console.log()
        const input = (sessionStorage.getItem('korisnik') != null) ? "" : 
                                                        <input
                                                            name="email"
                                                            value={this.state.email}
                                                            onChange={this.handleChange}
                                                            placeholder="Vaš email"
															pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                                                        />
        const disabled =    (sessionStorage.getItem('korisnik') != null ? false : this.state.erroremail.toString().length > 0) ||
                            this.state.errornaslov.toString().length > 0 || 
                            this.state.erroropis.toString().length > 0

                            console.log("mail" + this.state.mail)  
		let a = '';
		if(sessionStorage.getItem('korisnik') === null) a = <p><strong>Vaš email</strong></p>
        let b = '';
		if(sessionStorage.getItem('korisnik') === null) b = <p style={{color:"crimson", marginLeft:"10px", fontSize: "15px"}}> {this.state.erroremail} </p>
        return (
            <div>
               
                <form>
					{a}
                    {input}
                    {b}

					<p><strong>Naslov upita</strong></p>
                    <input
                        name="naslov"
                        value={this.state.naslov}
                        onChange={this.handleChange}
                        placeholder="Naslov"
                    />
                    <br />
					<p style={{color:"crimson", marginLeft:"10px", fontSize: "15px"}}> {this.state.errornaslov} </p>



                    <p><strong>Upit</strong></p>
                    <br />
                    <textarea name="opis" value={this.state.opis}
                        onChange={this.handleChange}
                        placeholder="Opis" cols="150" rows="5"></textarea>
					<p style={{color:"crimson", marginLeft:"10px", fontSize: "15px"}}> {this.state.erroropis} </p>


                </form>
           
                    
                <button  disabled={disabled}  onClick={this.posaljiUpit}> Posalji </button>
            </div>
        );
    }
}

export default withRouter(Upit)
