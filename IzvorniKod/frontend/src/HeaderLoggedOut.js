import React, { Component} from 'react';
import modal from './style.css';
import Modal from 'react-modal';
import PrijavaModal from './PrijavaModal';
import RegistracijaModal from './RegistracijaModal';
import { NavLink } from 'react-router-dom'

//import './SpecijalnaPonuda.css'
import './header.css'
import './App.css'

class HeaderLoggedOut extends Component{
    constructor(){
        super();
    };
    render(){
        return(
            <header >

                <h1 className="naslov">ZALAGAONICA</h1>
                <PrijavaModal className="prijavaModal" onLogin={this.props.onLogin}/>
                <RegistracijaModal className="registracijaModal" onLogin={this.props.onLogin}/>
                <div className="divLinkovi">
                    <NavLink exact to="/" className="inactiveNav" activeClassName="activeNav">Naslovnica</NavLink>
                    <NavLink exact to="/predmetiPonuda" className="inactiveNav" activeClassName="activeNav">Ponuda</NavLink>
                    <NavLink exact to="/upit" className="inactiveNav" activeClassName="activeNav"> Postavi upit </NavLink>
                </div>
            </header>
        );
    }
}
export default HeaderLoggedOut;
