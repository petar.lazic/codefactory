import React from 'react'
import './SpecijalnaPonuda.css'
import { withRouter } from "react-router-dom";

class Card extends React.Component{
    constructor(props){
		super(props);
	}
	
	
   render(){
    return (
        <div id={`card-${this.props.index}`} className="card">
			<img src={this.props.image} alt="greska" height="250" width="270"/>
			</div>
    )
	}
};



export default withRouter(Card);