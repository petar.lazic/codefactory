import React, { Component } from "react";

import PredmetContainer from "./PredmetContainer"

class SviPredmeti extends Component {
    constructor() {
        super()
        this.state = {
          predmeti: [],
          loaded: false
        }
      }
      componentDidMount(){
        this.dohvatiPredmete()
      }
  
      dohvatiPredmete = () => {
        fetch('http://localhost:8080/predmeti/', {
            method: 'GET',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Basic ' + sessionStorage.getItem('btoa')
            }})
        .then((response) => response.json()).then(response => {
          console.log(response);
          
          this.setState({
              predmeti: response,
              loaded: true
          })
      });
      }
  
      
      render() {
        if (!this.state.loaded) return <h1> loading... </h1>
        const predmeti = this.state.predmeti.map(p => <PredmetContainer image={"http://localhost:8080/files/files/" + p.slike[0].put} id={p.id} naziv={p.imePredmeta} datum={p.datumUvrstenja} cijena={p.cijena} uPonudi={p.uPonudi} posebnaPonuda={p.posebnaPonuda}  />)
        return (
          <div > 
            <table style={{width:"75%", marginLeft: "auto", marginRight: "auto", padding: "15px", textAlign: "center", borderSpacing: "0px", fontSize:"25px"}}>
                
                <tr >
                  <th style={{border: "1px solid black"}}>SLIKA</th>
                  <th style={{border: "1px solid black"}}>ID</th>
                  <th style={{border: "1px solid black"}}>NAZIV</th> 
                  <th style={{border: "1px solid black"}}>CIJENA (HRK)</th>
                  <th style={{border: "1px solid black"}}>DATUM DODAVANJA</th>
                </tr>
                {predmeti}
              </table>
          </div> 
        )
          
      }
  
  
}

export default SviPredmeti