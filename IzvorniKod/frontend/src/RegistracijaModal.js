import ReactModal from "react-modal"
import React from "react"

import './Modal.css'
import Registracija from './Registracija'

ReactModal.setAppElement(document.getElementById('root'));

class RegistracijaModal extends React.Component {
  constructor () {
    super();
    this.state = {
      showModal: false
    
    };
    
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }
  
  handleOpenModal (event) {
        this.setState({ showModal: true });
  }
  
  handleCloseModal (event) {
    this.setState({ showModal: false });
  }
  
  
  render () {
    return (
      <div className="divButtonRegPrijava">
        <button onClick={this.handleOpenModal} className="buttonRegPrijava">Registriraj se</button>
        <ReactModal 
           name="registracija"
           isOpen={this.state.showModal}
           className="Modal"
           contentLabel="showModalRegistracija"
           onRequestClose={this.handleCloseModal}
           shouldCloseOnOverlayClick={true}
        >
          <Registracija onLogin={this.props.onLogin}/>
          <button onClick={this.handleCloseModal} >Close</button>
          </ReactModal>
      </div>
    );
  }
}

const props = {};

export default RegistracijaModal;