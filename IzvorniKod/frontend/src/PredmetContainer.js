import React, { Component } from "react"
import { withRouter } from "react-router-dom";
import { NavLink } from 'react-router-dom'

class PredmetContainer extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            naziv: props.naziv,
            id: props.id,
            datum: props.datum,
            cijena: props.cijena,
            uPonudi: props.uPonudi,
            posebnaPonuda: props.posebnaPonuda,
            image: props.image
        }
    }
    handleClick = () => {
        this.props.history.push("/predmeti/" + this.state.id);
    }

    

    render() {
        const role = sessionStorage.getItem('uloga');
        const link = "/predmeti/" + this.state.id
        const link2 = "/izmjenaPredmeta/" + this.state.id
        return (
            <tr >
                <td style={{border: "1px solid black"}}> <img height="150" width="150" src={this.state.image} alt="greska" /> </td> 
                <td style={{border: "1px solid black"}}> {this.state.id} </td>
                <td style={{border: "1px solid black"}}> <NavLink exact to = {link} >{this.state.naziv}</NavLink> </td>
                <td style={{border: "1px solid black"}}> {this.state.cijena} </td>
                <td style={{border: "1px solid black"}}> {this.state.datum.substring(0,10)} </td>
                {this.state.uPonudi && (role === "ROLE_ADMIN" || role === "ROLE_DJELATNIK") ? <td style={{border: "1px solid black"}}> U PONUDI </td> : ""} 
                {role === "ROLE_ADMIN" || role === "ROLE_DJELATNIK" ? <td style={{border: "1px solid black"}}> <NavLink exact to = {link2} >IZMJENI</NavLink> </td> : ""} 
            
            </tr>
            
        )
    }
}


export default withRouter(PredmetContainer)