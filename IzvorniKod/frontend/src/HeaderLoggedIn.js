import React, {Component} from 'react';
import { NavLink } from 'react-router-dom'

import './header.css'
class HeaderLoggedIn extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        let registriraj = '';
        let dodajPredmet = '';
        let sviPredmeti = '';
        let upit = '';
        let upiti = '';
        let statistika = '';
        let pregledKorisnika = '';
		if(this.props.role === 'ROLE_ADMIN' || this.props.role === 'ROLE_DJELATNIK' ) {
			dodajPredmet = <NavLink exact to="/dodajPredmet" className="inactiveNav" activeClassName="activeNav">Dodaj predmet</NavLink>
            sviPredmeti = <NavLink exact to="/sviPredmeti" className="inactiveNav" activeClassName="activeNav">Predmeti</NavLink>
            upiti = <NavLink exact to="/upiti" className="inactiveNav" activeClassName="activeNav"> Upiti </NavLink>
			statistika = <NavLink exact to="/statistika" className="inactiveNav" activeClassName="activeNav"> Statistika </NavLink>
            
        }
        if(this.props.role === 'ROLE_ADMIN') {
            registriraj = <NavLink exact to = "/registriraj" className="inactiveNav" activeClassName="activeNav" >Registriraj</NavLink>
            pregledKorisnika = <NavLink exact to="/pregledKorisnika" className="inactiveNav" activeClassName="activeNav"> Pregled svih korisnika </NavLink>
                
        }
        if (this.props.role != 'ROLE_ADMIN' && this.props.role != 'ROLE_DJELATNIK') {
            upit = <NavLink exact to="/upit" className="inactiveNav" activeClassName="activeNav"> Postavi upit </NavLink>
        
        }
        return(
            <div>
                <header className = "HeaderLoggedIn">


                    <div className="trenutniKorisnik"> Korisnik: {this.props.name} <br />
                        <button className="odjavaButton" onClick={this.props.onLogout}>Odjavi se</button>
                    </div>
                    <h1 className="naslov">ZALAGAONICA</h1>
                    <div className="divLinkovi">
                        <NavLink exact to = "/profil" className="inactiveNav" activeClassName="activeNav" >Profil</NavLink><br />
                        <NavLink exact to="/" className="inactiveNav" activeClassName="activeNav">Naslovnica</NavLink> <br/>
                        <NavLink exact to="/predmetiPonuda" className="inactiveNav" activeClassName="activeNav">Ponuda</NavLink> <br/>


                    {registriraj}
                    {dodajPredmet}
                    {sviPredmeti}
                    {upiti}
                    {upit}
                    {statistika}
                    {pregledKorisnika}
                    </div>

			   </header>
            </div>
        )
    }
};

export default HeaderLoggedIn;
